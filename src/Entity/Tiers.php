<?php

namespace App\Entity;

use App\Repository\TiersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TiersRepository::class)
 */
class Tiers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="clients")
     */
    private $factures;

    /**
     * @ORM\ManyToOne(targetEntity=Categorietarifaire::class, inversedBy="tiers")
     */
    private $categorietarifaires;

    /**
     * @ORM\ManyToOne(targetEntity=Categoriecomptable::class, inversedBy="tiers")
     */
    private $categoriecomptables;

    /**
     * @ORM\ManyToOne(targetEntity=Taxe::class, inversedBy="tiers")
     */
    private $taxes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ifu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    public function __construct()
    {
        $this->factures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(?string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures(): Collection
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture): self
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setClients($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture): self
    {
        if ($this->factures->removeElement($facture)) {
            // set the owning side to null (unless already changed)
            if ($facture->getClients() === $this) {
                $facture->setClients(null);
            }
        }

        return $this;
    }

    public function getCategorietarifaires(): ?Categorietarifaire
    {
        return $this->categorietarifaires;
    }

    public function setCategorietarifaires(?Categorietarifaire $categorietarifaires): self
    {
        $this->categorietarifaires = $categorietarifaires;

        return $this;
    }

    public function getCategoriecomptables(): ?Categoriecomptable
    {
        return $this->categoriecomptables;
    }

    public function setCategoriecomptables(?Categoriecomptable $categoriecomptables): self
    {
        $this->categoriecomptables = $categoriecomptables;

        return $this;
    }

    public function getTaxes(): ?Taxe
    {
        return $this->taxes;
    }

    public function setTaxes(?Taxe $taxes): self
    {
        $this->taxes = $taxes;

        return $this;
    }

    public function getIfu(): ?string
    {
        return $this->ifu;
    }

    public function setIfu(?string $ifu): self
    {
        $this->ifu = $ifu;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}
