<?php

namespace App\Entity;

use App\Repository\CategoriecomptableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoriecomptableRepository::class)
 */
class Categoriecomptable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @ORM\OneToMany(targetEntity=Tiers::class, mappedBy="categoriecomptables")
     */
    private $tiers;

    /**
     * @ORM\OneToMany(targetEntity=Taxecategorie::class, mappedBy="categoriecomptables")
     */
    private $taxecategories;

    public function __construct()
    {
        $this->tiers = new ArrayCollection();
        $this->taxecategories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * @return Collection|Tiers[]
     */
    public function getTiers(): Collection
    {
        return $this->tiers;
    }

    public function addTier(Tiers $tier): self
    {
        if (!$this->tiers->contains($tier)) {
            $this->tiers[] = $tier;
            $tier->setCategoriecomptables($this);
        }

        return $this;
    }

    public function removeTier(Tiers $tier): self
    {
        if ($this->tiers->removeElement($tier)) {
            // set the owning side to null (unless already changed)
            if ($tier->getCategoriecomptables() === $this) {
                $tier->setCategoriecomptables(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Taxecategorie[]
     */
    public function getTaxecategories(): Collection
    {
        return $this->taxecategories;
    }

    public function addTaxecategory(Taxecategorie $taxecategory): self
    {
        if (!$this->taxecategories->contains($taxecategory)) {
            $this->taxecategories[] = $taxecategory;
            $taxecategory->setCategoriecomptables($this);
        }

        return $this;
    }

    public function removeTaxecategory(Taxecategorie $taxecategory): self
    {
        if ($this->taxecategories->removeElement($taxecategory)) {
            // set the owning side to null (unless already changed)
            if ($taxecategory->getCategoriecomptables() === $this) {
                $taxecategory->setCategoriecomptables(null);
            }
        }

        return $this;
    }
}
