<?php

namespace App\Entity;

use App\Repository\DepotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DepotRepository::class)
 */
class Depot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPrincipal;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="depots")
     */
    private $stocks;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="depotrecepteur")
     */
    private $depotrecepteurs;

    /**
     * @ORM\OneToMany(targetEntity=Lignestock::class, mappedBy="depotrecepteurs")
     */
    private $lignestocks;

    /**
     * @ORM\OneToMany(targetEntity=Tamponlignefacture::class, mappedBy="depots")
     */
    private $tamponlignefactures;

    /**
     * @ORM\OneToMany(targetEntity=Lignefacture::class, mappedBy="depots")
     */
    private $lignefactures;

    public function __construct()
    {
        $this->stocks = new ArrayCollection();
        $this->depotrecepteurs = new ArrayCollection();
        $this->lignestocks = new ArrayCollection();
        $this->tamponlignefactures = new ArrayCollection();
        $this->lignefactures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getIsPrincipal(): ?bool
    {
        return $this->isPrincipal;
    }

    public function setIsPrincipal(?bool $isPrincipal): self
    {
        $this->isPrincipal = $isPrincipal;

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setDepots($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getDepots() === $this) {
                $stock->setDepots(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getDepotrecepteurs(): Collection
    {
        return $this->depotrecepteurs;
    }

    public function addDepotrecepteur(Stock $depotrecepteur): self
    {
        if (!$this->depotrecepteurs->contains($depotrecepteur)) {
            $this->depotrecepteurs[] = $depotrecepteur;
            $depotrecepteur->setDepotrecepteur($this);
        }

        return $this;
    }

    public function removeDepotrecepteur(Stock $depotrecepteur): self
    {
        if ($this->depotrecepteurs->removeElement($depotrecepteur)) {
            // set the owning side to null (unless already changed)
            if ($depotrecepteur->getDepotrecepteur() === $this) {
                $depotrecepteur->setDepotrecepteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lignestock[]
     */
    public function getLignestocks(): Collection
    {
        return $this->lignestocks;
    }

    public function addLignestock(Lignestock $lignestock): self
    {
        if (!$this->lignestocks->contains($lignestock)) {
            $this->lignestocks[] = $lignestock;
            $lignestock->setDepotrecepteurs($this);
        }

        return $this;
    }

    public function removeLignestock(Lignestock $lignestock): self
    {
        if ($this->lignestocks->removeElement($lignestock)) {
            // set the owning side to null (unless already changed)
            if ($lignestock->getDepotrecepteurs() === $this) {
                $lignestock->setDepotrecepteurs(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tamponlignefacture[]
     */
    public function getTamponlignefactures(): Collection
    {
        return $this->tamponlignefactures;
    }

    public function addTamponlignefacture(Tamponlignefacture $tamponlignefacture): self
    {
        if (!$this->tamponlignefactures->contains($tamponlignefacture)) {
            $this->tamponlignefactures[] = $tamponlignefacture;
            $tamponlignefacture->setDepots($this);
        }

        return $this;
    }

    public function removeTamponlignefacture(Tamponlignefacture $tamponlignefacture): self
    {
        if ($this->tamponlignefactures->removeElement($tamponlignefacture)) {
            // set the owning side to null (unless already changed)
            if ($tamponlignefacture->getDepots() === $this) {
                $tamponlignefacture->setDepots(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lignefacture[]
     */
    public function getLignefactures(): Collection
    {
        return $this->lignefactures;
    }

    public function addLignefacture(Lignefacture $lignefacture): self
    {
        if (!$this->lignefactures->contains($lignefacture)) {
            $this->lignefactures[] = $lignefacture;
            $lignefacture->setDepots($this);
        }

        return $this;
    }

    public function removeLignefacture(Lignefacture $lignefacture): self
    {
        if ($this->lignefactures->removeElement($lignefacture)) {
            // set the owning side to null (unless already changed)
            if ($lignefacture->getDepots() === $this) {
                $lignefacture->setDepots(null);
            }
        }

        return $this;
    }
}
