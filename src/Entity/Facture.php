<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numeroFacture;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prixht;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prixttc;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantRemise;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFacture;

    /**
     * @ORM\OneToMany(targetEntity=Lignefacture::class, mappedBy="factures")
     */
    private $lignefactures;

    /**
     * @ORM\ManyToOne(targetEntity=Tiers::class, inversedBy="factures")
     */
    private $clients;

    /**
     * @ORM\ManyToOne(targetEntity=Typefacture::class, inversedBy="factures")
     */
    private $typefactures;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="factures")
     */
    private $reglements;

    /**
     * @ORM\OneToMany(targetEntity=Tamponlignefacture::class, mappedBy="factures")
     */
    private $tamponlignefactures;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPayer = false;

    public function __construct()
    {
        $this->lignefactures = new ArrayCollection();
        $this->reglements = new ArrayCollection();
        $this->tamponlignefactures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getNumeroFacture(): ?string
    {
        return $this->numeroFacture;
    }

    public function setNumeroFacture(?string $numeroFacture): self
    {
        $this->numeroFacture = $numeroFacture;

        return $this;
    }

    public function getPrixht(): ?float
    {
        return $this->prixht;
    }

    public function setPrixht(?float $prixht): self
    {
        $this->prixht = $prixht;

        return $this;
    }

    public function getPrixttc(): ?float
    {
        return $this->prixttc;
    }

    public function setPrixttc(?float $prixttc): self
    {
        $this->prixttc = $prixttc;

        return $this;
    }

    public function getMontantRemise(): ?float
    {
        return $this->montantRemise;
    }

    public function setMontantRemise(?float $montantRemise): self
    {
        $this->montantRemise = $montantRemise;

        return $this;
    }

    public function getDateFacture(): ?\DateTimeInterface
    {
        return $this->dateFacture;
    }

    public function setDateFacture(?\DateTimeInterface $dateFacture): self
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    /**
     * @return Collection|Lignefacture[]
     */
    public function getLignefactures(): Collection
    {
        return $this->lignefactures;
    }

    public function addLignefacture(Lignefacture $lignefacture): self
    {
        if (!$this->lignefactures->contains($lignefacture)) {
            $this->lignefactures[] = $lignefacture;
            $lignefacture->setFactures($this);
        }

        return $this;
    }

    public function removeLignefacture(Lignefacture $lignefacture): self
    {
        if ($this->lignefactures->removeElement($lignefacture)) {
            // set the owning side to null (unless already changed)
            if ($lignefacture->getFactures() === $this) {
                $lignefacture->setFactures(null);
            }
        }

        return $this;
    }

    public function getClients(): ?Tiers
    {
        return $this->clients;
    }

    public function setClients(?Tiers $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    public function getTypefactures(): ?Typefacture
    {
        return $this->typefactures;
    }

    public function setTypefactures(?Typefacture $typefactures): self
    {
        $this->typefactures = $typefactures;

        return $this;
    }

    /**
     * @return Collection|Reglement[]
     */
    public function getReglements(): Collection
    {
        return $this->reglements;
    }

    public function addReglement(Reglement $reglement): self
    {
        if (!$this->reglements->contains($reglement)) {
            $this->reglements[] = $reglement;
            $reglement->setFactures($this);
        }

        return $this;
    }

    public function removeReglement(Reglement $reglement): self
    {
        if ($this->reglements->removeElement($reglement)) {
            // set the owning side to null (unless already changed)
            if ($reglement->getFactures() === $this) {
                $reglement->setFactures(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tamponlignefacture[]
     */
    public function getTamponlignefactures(): Collection
    {
        return $this->tamponlignefactures;
    }

    public function addTamponlignefacture(Tamponlignefacture $tamponlignefacture): self
    {
        if (!$this->tamponlignefactures->contains($tamponlignefacture)) {
            $this->tamponlignefactures[] = $tamponlignefacture;
            $tamponlignefacture->setFactures($this);
        }

        return $this;
    }

    public function removeTamponlignefacture(Tamponlignefacture $tamponlignefacture): self
    {
        if ($this->tamponlignefactures->removeElement($tamponlignefacture)) {
            // set the owning side to null (unless already changed)
            if ($tamponlignefacture->getFactures() === $this) {
                $tamponlignefacture->setFactures(null);
            }
        }

        return $this;
    }

    public function getIsPayer(): ?bool
    {
        return $this->isPayer;
    }

    public function setIsPayer(?bool $isPayer): self
    {
        $this->isPayer = $isPayer;

        return $this;
    }
}
