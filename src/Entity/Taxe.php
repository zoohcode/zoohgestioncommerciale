<?php

namespace App\Entity;

use App\Repository\TaxeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaxeRepository::class)
 */
class Taxe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $taux;

    /**
     * @ORM\OneToMany(targetEntity=Taxecategorie::class, mappedBy="taxes")
     */
    private $taxecategories;

    /**
     * @ORM\OneToMany(targetEntity=Tiers::class, mappedBy="taxes")
     */
    private $tiers;

    public function __construct()
    {
        $this->taxecategories = new ArrayCollection();
        $this->tiers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getTaux(): ?float
    {
        return $this->taux;
    }

    public function setTaux(?float $taux): self
    {
        $this->taux = $taux;

        return $this;
    }

    /**
     * @return Collection|Taxecategorie[]
     */
    public function getTaxecategories(): Collection
    {
        return $this->taxecategories;
    }

    public function addTaxecategory(Taxecategorie $taxecategory): self
    {
        if (!$this->taxecategories->contains($taxecategory)) {
            $this->taxecategories[] = $taxecategory;
            $taxecategory->setTaxes($this);
        }

        return $this;
    }

    public function removeTaxecategory(Taxecategorie $taxecategory): self
    {
        if ($this->taxecategories->removeElement($taxecategory)) {
            // set the owning side to null (unless already changed)
            if ($taxecategory->getTaxes() === $this) {
                $taxecategory->setTaxes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tiers[]
     */
    public function getTiers(): Collection
    {
        return $this->tiers;
    }

    public function addTier(Tiers $tier): self
    {
        if (!$this->tiers->contains($tier)) {
            $this->tiers[] = $tier;
            $tier->setTaxes($this);
        }

        return $this;
    }

    public function removeTier(Tiers $tier): self
    {
        if ($this->tiers->removeElement($tier)) {
            // set the owning side to null (unless already changed)
            if ($tier->getTaxes() === $this) {
                $tier->setTaxes(null);
            }
        }

        return $this;
    }
}
