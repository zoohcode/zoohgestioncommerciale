<?php

namespace App\Entity;

use App\Repository\TarifRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifRepository::class)
 */
class Tarif
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="tarifs")
     */
    private $articles;

    /**
     * @ORM\ManyToOne(targetEntity=Categorietarifaire::class, inversedBy="tarifs")
     */
    private $categorietarifaires;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $natureprix;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticles(): ?Article
    {
        return $this->articles;
    }

    public function setArticles(?Article $articles): self
    {
        $this->articles = $articles;

        return $this;
    }

    public function getCategorietarifaires(): ?Categorietarifaire
    {
        return $this->categorietarifaires;
    }

    public function setCategorietarifaires(?Categorietarifaire $categorietarifaires): self
    {
        $this->categorietarifaires = $categorietarifaires;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getNatureprix(): ?string
    {
        return $this->natureprix;
    }

    public function setNatureprix(?string $natureprix): self
    {
        $this->natureprix = $natureprix;

        return $this;
    }
}
