<?php

namespace App\Entity;

use App\Repository\TamponlignefactureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TamponlignefactureRepository::class)
 */
class Tamponlignefacture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Facture::class, inversedBy="tamponlignefactures")
     */
    private $factures;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="tamponlignefactures")
     */
    private $articles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quantite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $montant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prixunitaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prixht;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $totalht;

    /**
     * @ORM\ManyToOne(targetEntity=Depot::class, inversedBy="tamponlignefactures")
     */
    private $depots;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $totalaib1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $totalaib2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $montantht;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFactures(): ?Facture
    {
        return $this->factures;
    }

    public function setFactures(?Facture $factures): self
    {
        $this->factures = $factures;

        return $this;
    }

    public function getArticles(): ?Article
    {
        return $this->articles;
    }

    public function setArticles(?Article $articles): self
    {
        $this->articles = $articles;

        return $this;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(?string $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getMontant(): ?string
    {
        return $this->montant;
    }

    public function setMontant(?string $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getPrixunitaire(): ?string
    {
        return $this->prixunitaire;
    }

    public function setPrixunitaire(?string $prixunitaire): self
    {
        $this->prixunitaire = $prixunitaire;

        return $this;
    }

    public function getPrixht(): ?string
    {
        return $this->prixht;
    }

    public function setPrixht(?string $prixht): self
    {
        $this->prixht = $prixht;

        return $this;
    }

    public function getTotalht(): ?string
    {
        return $this->totalht;
    }

    public function setTotalht(?string $totalht): self
    {
        $this->totalht = $totalht;

        return $this;
    }

    public function getDepots(): ?Depot
    {
        return $this->depots;
    }

    public function setDepots(?Depot $depots): self
    {
        $this->depots = $depots;

        return $this;
    }

    public function getTotalaib1(): ?string
    {
        return $this->totalaib1;
    }

    public function setTotalaib1(?string $totalaib1): self
    {
        $this->totalaib1 = $totalaib1;

        return $this;
    }

    public function getTotalaib2(): ?string
    {
        return $this->totalaib2;
    }

    public function setTotalaib2(?string $totalaib2): self
    {
        $this->totalaib2 = $totalaib2;

        return $this;
    }

    public function getMontantht(): ?string
    {
        return $this->montantht;
    }

    public function setMontantht(?string $montantht): self
    {
        $this->montantht = $montantht;

        return $this;
    }
}
