<?php

namespace App\Entity;

use App\Repository\TaxecategorieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaxecategorieRepository::class)
 */
class Taxecategorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Taxe::class, inversedBy="taxecategories")
     */
    private $taxes;

    /**
     * @ORM\ManyToOne(targetEntity=Categoriecomptable::class, inversedBy="taxecategories")
     */
    private $categoriecomptables;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaxes(): ?Taxe
    {
        return $this->taxes;
    }

    public function setTaxes(?Taxe $taxes): self
    {
        $this->taxes = $taxes;

        return $this;
    }

    public function getCategoriecomptables(): ?Categoriecomptable
    {
        return $this->categoriecomptables;
    }

    public function setCategoriecomptables(?Categoriecomptable $categoriecomptables): self
    {
        $this->categoriecomptables = $categoriecomptables;

        return $this;
    }
}
