<?php

namespace App\Entity;

use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quantite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nlot;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stockReel;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="stocks")
     */
    private $articles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $refStock;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nserie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cmup;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $operation;

    /**
     * @ORM\OneToMany(targetEntity=Lignestock::class, mappedBy="stocks")
     */
    private $lignestocks;

    /**
     * @ORM\OneToMany(targetEntity=Tampon::class, mappedBy="stocks")
     */
    private $tampons;

    /**
     * @ORM\ManyToOne(targetEntity=Depot::class, inversedBy="stocks")
     */
    private $depots;

    /**
     * @ORM\ManyToOne(targetEntity=Depot::class, inversedBy="depotrecepteurs")
     */
    private $depotrecepteur;

    public function __construct()
    {
        $this->lignestocks = new ArrayCollection();
        $this->tampons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(?string $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getNlot(): ?string
    {
        return $this->nlot;
    }

    public function setNlot(?string $nlot): self
    {
        $this->nlot = $nlot;

        return $this;
    }

    public function getStockReel(): ?string
    {
        return $this->stockReel;
    }

    public function setStockReel(?string $stockReel): self
    {
        $this->stockReel = $stockReel;

        return $this;
    }

    public function getArticles(): ?Article
    {
        return $this->articles;
    }

    public function setArticles(?Article $articles): self
    {
        $this->articles = $articles;

        return $this;
    }

    public function getRefStock(): ?string
    {
        return $this->refStock;
    }

    public function setRefStock(?string $refStock): self
    {
        $this->refStock = $refStock;

        return $this;
    }

    public function getNserie(): ?string
    {
        return $this->nserie;
    }

    public function setNserie(?string $nserie): self
    {
        $this->nserie = $nserie;

        return $this;
    }

    public function getCmup(): ?string
    {
        return $this->cmup;
    }

    public function setCmup(?string $cmup): self
    {
        $this->cmup = $cmup;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getOperation(): ?string
    {
        return $this->operation;
    }

    public function setOperation(?string $operation): self
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * @return Collection|Lignestock[]
     */
    public function getLignestocks(): Collection
    {
        return $this->lignestocks;
    }

    public function addLignestock(Lignestock $lignestock): self
    {
        if (!$this->lignestocks->contains($lignestock)) {
            $this->lignestocks[] = $lignestock;
            $lignestock->setStocks($this);
        }

        return $this;
    }

    public function removeLignestock(Lignestock $lignestock): self
    {
        if ($this->lignestocks->removeElement($lignestock)) {
            // set the owning side to null (unless already changed)
            if ($lignestock->getStocks() === $this) {
                $lignestock->setStocks(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tampon[]
     */
    public function getTampons(): Collection
    {
        return $this->tampons;
    }

    public function addTampon(Tampon $tampon): self
    {
        if (!$this->tampons->contains($tampon)) {
            $this->tampons[] = $tampon;
            $tampon->setStocks($this);
        }

        return $this;
    }

    public function removeTampon(Tampon $tampon): self
    {
        if ($this->tampons->removeElement($tampon)) {
            // set the owning side to null (unless already changed)
            if ($tampon->getStocks() === $this) {
                $tampon->setStocks(null);
            }
        }

        return $this;
    }

    public function getDepots(): ?Depot
    {
        return $this->depots;
    }

    public function setDepots(?Depot $depots): self
    {
        $this->depots = $depots;

        return $this;
    }

    public function getDepotrecepteur(): ?Depot
    {
        return $this->depotrecepteur;
    }

    public function setDepotrecepteur(?Depot $depotrecepteur): self
    {
        $this->depotrecepteur = $depotrecepteur;

        return $this;
    }
}
