<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isStocknegatif = false;

    /**
     * @ORM\ManyToOne(targetEntity=Famille::class, inversedBy="articles")
     */
    private $familles;

    /**
     * @ORM\ManyToOne(targetEntity=Typesuivi::class, inversedBy="articles")
     */
    private $typesuivis;

    /**
     * @ORM\OneToMany(targetEntity=Lignefacture::class, mappedBy="articles")
     */
    private $lignefactures;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $statut = false;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="articles")
     */
    private $stocks;

    /**
     * @ORM\OneToMany(targetEntity=Tamponlignefacture::class, mappedBy="articles")
     */
    private $tamponlignefactures;

    /**
     * @ORM\OneToMany(targetEntity=Tarif::class, mappedBy="articles")
     */
    private $tarifs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prixventeht;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prixventettc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    public function __construct()
    {
        $this->lignefactures = new ArrayCollection();
        $this->stocks = new ArrayCollection();
        $this->tamponlignefactures = new ArrayCollection();
        $this->tarifs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(?string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getIsStocknegatif(): ?bool
    {
        return $this->isStocknegatif;
    }

    public function setIsStocknegatif(?bool $isStocknegatif): self
    {
        $this->isStocknegatif = $isStocknegatif;

        return $this;
    }

    public function getFamilles(): ?Famille
    {
        return $this->familles;
    }

    public function setFamilles(?Famille $familles): self
    {
        $this->familles = $familles;

        return $this;
    }

    public function getTypesuivis(): ?Typesuivi
    {
        return $this->typesuivis;
    }

    public function setTypesuivis(?Typesuivi $typesuivis): self
    {
        $this->typesuivis = $typesuivis;

        return $this;
    }

    /**
     * @return Collection|Lignefacture[]
     */
    public function getLignefactures(): Collection
    {
        return $this->lignefactures;
    }

    public function addLignefacture(Lignefacture $lignefacture): self
    {
        if (!$this->lignefactures->contains($lignefacture)) {
            $this->lignefactures[] = $lignefacture;
            $lignefacture->setArticles($this);
        }

        return $this;
    }

    public function removeLignefacture(Lignefacture $lignefacture): self
    {
        if ($this->lignefactures->removeElement($lignefacture)) {
            // set the owning side to null (unless already changed)
            if ($lignefacture->getArticles() === $this) {
                $lignefacture->setArticles(null);
            }
        }

        return $this;
    }

    public function getStatut(): ?bool
    {
        return $this->statut;
    }

    public function setStatut(?bool $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setArticles($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getArticles() === $this) {
                $stock->setArticles(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tamponlignefacture[]
     */
    public function getTamponlignefactures(): Collection
    {
        return $this->tamponlignefactures;
    }

    public function addTamponlignefacture(Tamponlignefacture $tamponlignefacture): self
    {
        if (!$this->tamponlignefactures->contains($tamponlignefacture)) {
            $this->tamponlignefactures[] = $tamponlignefacture;
            $tamponlignefacture->setArticles($this);
        }

        return $this;
    }

    public function removeTamponlignefacture(Tamponlignefacture $tamponlignefacture): self
    {
        if ($this->tamponlignefactures->removeElement($tamponlignefacture)) {
            // set the owning side to null (unless already changed)
            if ($tamponlignefacture->getArticles() === $this) {
                $tamponlignefacture->setArticles(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tarif[]
     */
    public function getTarifs(): Collection
    {
        return $this->tarifs;
    }

    public function addTarif(Tarif $tarif): self
    {
        if (!$this->tarifs->contains($tarif)) {
            $this->tarifs[] = $tarif;
            $tarif->setArticles($this);
        }

        return $this;
    }

    public function removeTarif(Tarif $tarif): self
    {
        if ($this->tarifs->removeElement($tarif)) {
            // set the owning side to null (unless already changed)
            if ($tarif->getArticles() === $this) {
                $tarif->setArticles(null);
            }
        }

        return $this;
    }

    public function getPrixventeht(): ?string
    {
        return $this->prixventeht;
    }

    public function setPrixventeht(?string $prixventeht): self
    {
        $this->prixventeht = $prixventeht;

        return $this;
    }

    public function getPrixventettc(): ?string
    {
        return $this->prixventettc;
    }

    public function setPrixventettc(?string $prixventettc): self
    {
        $this->prixventettc = $prixventettc;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}
