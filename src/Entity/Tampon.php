<?php

namespace App\Entity;

use App\Repository\TamponRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TamponRepository::class)
 */
class Tampon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Stock::class, inversedBy="tampons")
     */
    private $stocks;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nlot;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quantite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nserie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $article;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $typeoperation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $qtedispo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prixUnitaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prix;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStocks(): ?Stock
    {
        return $this->stocks;
    }

    public function setStocks(?Stock $stocks): self
    {
        $this->stocks = $stocks;

        return $this;
    }

    public function getNlot(): ?string
    {
        return $this->nlot;
    }

    public function setNlot(?string $nlot): self
    {
        $this->nlot = $nlot;

        return $this;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(?string $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getNserie(): ?string
    {
        return $this->nserie;
    }

    public function setNserie(?string $nserie): self
    {
        $this->nserie = $nserie;

        return $this;
    }

    public function getArticle(): ?string
    {
        return $this->article;
    }

    public function setArticle(?string $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getTypeoperation(): ?int
    {
        return $this->typeoperation;
    }

    public function setTypeoperation(?int $typeoperation): self
    {
        $this->typeoperation = $typeoperation;

        return $this;
    }

    public function getQtedispo(): ?float
    {
        return $this->qtedispo;
    }

    public function setQtedispo(?float $qtedispo): self
    {
        $this->qtedispo = $qtedispo;

        return $this;
    }

    public function getPrixUnitaire(): ?float
    {
        return $this->prixUnitaire;
    }

    public function setPrixUnitaire(?float $prixUnitaire): self
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
}
