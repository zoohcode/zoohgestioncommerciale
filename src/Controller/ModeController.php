<?php

namespace App\Controller;

use App\Entity\Mode;
use App\Form\ModeType;
use App\Repository\ModeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mode")
 */
class ModeController extends AbstractController
{
    /**
     * @Route("/", name="mode_index", methods={"GET"})
     */
    public function index(ModeRepository $modeRepository): Response
    {
        return $this->render('mode/index.html.twig', [
            'modes' => $modeRepository->findBy(['statut' => false]),
        ]);
    }

    /**
     * @Route("/new", name="mode_new", methods={"GET","POST"})
     */
    public function new(Request $request, ModeRepository $modeRepository): Response
    {

        $mode = new Mode();
        $modeold = $modeRepository->findBy(['intitule' => $request->request->get('intitule1')]);
        if ($modeold) {
            $this->addFlash('error', 'Ce mode de reglement existe déjà');
            return $this->redirectToRoute('mode_index');
        } else {
            $mode->setIntitule($request->request->get('intitule1'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mode);
            $entityManager->flush();
            $this->addFlash('success', 'Mode de règlement crée avec success!');
            return $this->redirectToRoute('mode_index');
        }
        
    }

    /**
     * @Route("/{id}", name="mode_show", methods={"GET"})
     */
    public function show(Mode $mode): Response
    {
        return $this->render('mode/show.html.twig', [
            'mode' => $mode,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mode_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mode $mode): Response
    {

        $intitule = $request->request->get('intitule2');
        $mode->setIntitule($intitule);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Mode de règlement modifié avec success!');
        return $this->redirectToRoute('mode_index');
    }

    /**
     * @Route("/{id}", name="mode_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Mode $mode): Response
    {
        $mode->setStatut(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($mode);
        $entityManager->flush();

        return $this->redirectToRoute('mode_index');
    }
}
