<?php

namespace App\Controller;

use App\Entity\Tampon;
use App\Form\TamponType;
use App\Repository\TamponRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tampon")
 */
class TamponController extends AbstractController
{
    /**
     * @Route("/", name="tampon_index", methods={"GET"})
     */
    public function index(TamponRepository $tamponRepository): Response
    {
        return $this->render('tampon/index.html.twig', [
            'tampons' => $tamponRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tampon_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tampon = new Tampon();
        $form = $this->createForm(TamponType::class, $tampon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tampon);
            $entityManager->flush();

            return $this->redirectToRoute('tampon_index');
        }

        return $this->render('tampon/new.html.twig', [
            'tampon' => $tampon,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tampon_show", methods={"GET"})
     */
    public function show(Tampon $tampon): Response
    {
        return $this->render('tampon/show.html.twig', [
            'tampon' => $tampon,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tampon_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tampon $tampon): Response
    {
        $form = $this->createForm(TamponType::class, $tampon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tampon_index');
        }

        return $this->render('tampon/edit.html.twig', [
            'tampon' => $tampon,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tampon_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tampon $tampon): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tampon->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tampon);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tampon_index');
    }
}
