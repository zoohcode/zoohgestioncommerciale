<?php

namespace App\Controller;

use App\Entity\Typesuivi;
use App\Form\TypesuiviType;
use App\Repository\TypesuiviRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/typesuivi")
 */
class TypesuiviController extends AbstractController
{
    /**
     * @Route("/", name="typesuivi_index", methods={"GET"})
     */
    public function index(TypesuiviRepository $typesuiviRepository): Response
    {
        return $this->render('typesuivi/index.html.twig', [
            'typesuivis' => $typesuiviRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="typesuivi_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typesuivi = new Typesuivi();
        $form = $this->createForm(TypesuiviType::class, $typesuivi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typesuivi);
            $entityManager->flush();

            return $this->redirectToRoute('typesuivi_index');
        }

        return $this->render('typesuivi/new.html.twig', [
            'typesuivi' => $typesuivi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="typesuivi_show", methods={"GET"})
     */
    public function show(Typesuivi $typesuivi): Response
    {
        return $this->render('typesuivi/show.html.twig', [
            'typesuivi' => $typesuivi,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="typesuivi_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Typesuivi $typesuivi): Response
    {
        $form = $this->createForm(TypesuiviType::class, $typesuivi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typesuivi_index');
        }

        return $this->render('typesuivi/edit.html.twig', [
            'typesuivi' => $typesuivi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="typesuivi_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Typesuivi $typesuivi): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typesuivi->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typesuivi);
            $entityManager->flush();
        }

        return $this->redirectToRoute('typesuivi_index');
    }
}
