<?php

namespace App\Controller;

use App\Entity\Stock;
use App\Entity\Tampon;
use App\Form\StockType;
use App\Entity\Lignestock;
use Doctrine\ORM\EntityManager;
use App\Repository\DepotRepository;
use App\Repository\StockRepository;
use Doctrine\DBAL\Driver\Exception;
use App\Repository\TamponRepository;
use App\Repository\ArticleRepository;
use App\Repository\LignestockRepository;
use App\Repository\TypesuiviRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/stock")
 */
class StockController extends AbstractController
{

    /**
     * @Route("/getLotDispo", name="getLotDispo")
     */
    public function qteLotDispo(Request $request, LignestockRepository $lignestockRepository)
    {
        $exist = $lignestockRepository->findOneBy(['nlot' => $request->request->get('nlot')], ['id' => 'DESC']);
        $data =  $exist->getQtedispo();
        return new JsonResponse(['data' => $data]);
    }

    /**
     * @Route("/getqteDispo", name="getqteDispo")
     */
    public function getqteDispo(ArticleRepository $articleRepository, Request $request, LignestockRepository $lignestockRepository)
    {
        $type = $articleRepository->findOneBy(['designation' => $request->request->get('article')]);
        if ($type->getTypesuivis()->getIntitule() == "CMUP") {
            $exist = $lignestockRepository->articlecmup(
                $request->request->get('article'),
                $request->request->get('depot')
            );

            $data =  $exist[0]['qtedispo'];
            return new JsonResponse(['data' => $data]);
        }
        if ($type->getTypesuivis()->getIntitule() == "SERIE") {
            $exist = $lignestockRepository->articleserie(
                $request->request->get('article'),
                $request->request->get('depot')
            );
            $data =  $exist[0]['article'];
            return new JsonResponse(['data' => $data]);
        }

        if ($type->getTypesuivis()->getIntitule() == "LOT") {
            $exist = $lignestockRepository->articlelot(
                $request->request->get('article'),
                $request->request->get('depot')
            );
            $data =  $exist[0]['qte'];
            $type = $type->getTypesuivis()->getIntitule() ;
           // dump($data, $type);
            return new JsonResponse(['data' => $data , 'type' => $type]);
        }
    }

    /**
     * @Route("/", name="stock_index", methods={"GET"})
     */
    public function index(StockRepository $stockRepository, ArticleRepository $articleRepository, DepotRepository $depotRepository): Response
    {
        // dd($stockRepository->findOneBy([])->getArticles()->getTypesuivis()->getIntitule());
        return $this->render('stock/index.html.twig', [
            'stocks' => $stockRepository->findAll(),
            'articles' => $articleRepository->findAll(),
            'depots' => $depotRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="stock_new", methods={"GET","POST"})
     */
    public function new(Request $request, LignestockRepository $lignestockRepository, ArticleRepository $articleRepository, StockRepository $stockRepository, DepotRepository $depotRepository)
    {

        $stock = new Stock();
        $lignestock = new Lignestock();
        if ($request->request->get('operation') == 1) {

            $article = $articleRepository->find($request->request->get('article'));
            $type = $article->getTypesuivis()->getIntitule();
            $olddep = $depotRepository->find($request->request->get('depot'));

            $oldligne = $lignestockRepository->findOneBy(['article' => $article->getDesignation(), 'depot' => $olddep->getId()], ['id' => 'DESC']);

            if ($type == "CMUP") {
                $num = 'MVE' . sprintf("%'.012d", $stockRepository->findOneBy([], ['id' => 'DESC'])  ? ($stockRepository->findOneBy([], ['id' => 'DESC']))->getId() + 1 : 1);
                $stock->setArticles($article);
                $stock->setDepots($depotRepository->find($request->request->get('depot')));
                $stock->setQuantite($request->request->get('quantite'));
                if ($stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC']) == null) {
                    $stock->setStockReel($request->request->get('quantite'));
                } else {
                    $sreelF = $request->request->get('quantite') + $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel();
                    $stock->setStockReel($sreelF);
                }
                $stock->setRefStock($num);
                $stock->setCreatedAt(new \DateTime($request->request->get('date')));
                $stock->setOperation($request->request->get('operation'));



                $lignestock->setQuantite($request->request->get('quantite'));

                if ($oldligne == null) {
                    $lignestock->setQtedispo($request->request->get('quantite'));
                } else {
                    $lignestock->setQtedispo($request->request->get('quantite') + $oldligne->getQtedispo());
                }
                $lignestock->setStocks($stock);
                $lignestock->setDepot($stock->getDepots()->getId());
                $lignestock->setTypeoperation($stock->getOperation());
                $lignestock->setArticle($stock->getArticles()->getDesignation());


                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($stock);
                $entityManager->persist($lignestock);
                $entityManager->flush();
                return $this->redirectToRoute('stock_index');
            }

            if ($type == "LOT") {

                $num = 'MVE' . sprintf("%'.012d", $stockRepository->findOneBy([], ['id' => 'DESC'])  ? ($stockRepository->findOneBy([], ['id' => 'DESC']))->getId() + 1 : 1);

                $stock->setDepots($depotRepository->find($request->request->get('depot')));
                $stock->setArticles($article);
                $stock->setQuantite($request->request->get('quantite'));
                $stock->setRefStock($num);
                if ($stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC']) == null) {
                    $stock->setStockReel($request->request->get('quantite'));
                } else {
                    $sreelF = $request->request->get('quantite') + $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel();
                    $stock->setStockReel($sreelF);
                }
                $stock->setCreatedAt(new \DateTime($request->request->get('date')));
                $stock->setOperation($request->request->get('operation'));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($stock);
                $entityManager->flush();
                return $this->redirectToRoute('entreelot', [
                    'id' => $stock->getId()
                ]);
            }

            if ($type == "SERIE") {

                $num = 'MVE' . sprintf("%'.012d", $stockRepository->findOneBy([], ['id' => 'DESC'])  ? ($stockRepository->findOneBy([], ['id' => 'DESC']))->getId() + 1 : 1);

                $stock->setDepots($depotRepository->find($request->request->get('depot')));
                $stock->setArticles($article);
                $stock->setQuantite($request->request->get('quantite'));
                $stock->setRefStock($num);
                if ($stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC']) == null) {
                    $stock->setStockReel($request->request->get('quantite'));
                } else {
                    $sreelF = $request->request->get('quantite') + $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel();
                    $stock->setStockReel($sreelF);
                }
                $stock->setCreatedAt(new \DateTime($request->request->get('date')));
                $stock->setOperation($request->request->get('operation'));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($stock);
                $entityManager->flush();
                return $this->redirectToRoute('entreeserie', [
                    'id' => $stock->getId()
                ]);
            }
        }

        if ($request->request->get('operation') == 2) {

            $article = $articleRepository->find($request->request->get('article'));
            $type = $article->getTypesuivis()->getIntitule();

            if ($type == "CMUP") {

                $num = 'MVS' . sprintf("%'.012d", $stockRepository->findOneBy([], ['id' => 'DESC'])  ? ($stockRepository->findOneBy([], ['id' => 'DESC']))->getId() + 1 : 1);
                $stock->setArticles($article);
                $stock->setDepots($depotRepository->find($request->request->get('depot')));
                $stock->setQuantite($request->request->get('quantite'));
                if ($stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC']) == null) {
                    $sortie = 0 - $request->request->get('quantite');
                    if ($article->getIsStocknegatif() == true) {
                        $stock->setStockReel($sortie);
                    } else {
                        if ($stock->setStockReel($sortie)->getStockReel()  < 0) {
                            $this->addFlash('error', 'Impossible d\' éffectuer cette opération il vous reste 0 en stock');
                            return $this->redirectToRoute('stock_index');
                        }
                    }
                } else {
                    $sreelF =  $stockRepository->findOneBy(['articles' => $article, 'depots' => $stock->getDepots()->getId()], ['id' => 'DESC'])->getStockReel() -  $request->request->get('quantite');

                    if ($article->getIsStocknegatif() == true) {
                        $stock->setStockReel($sreelF);
                    } else {
                        if ($stock->setStockReel($sreelF)->getStockReel() < 0) {
                            $this->addFlash('error', 'Impossible d\' éffectuer cette opération il vous reste ' . $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel() . ' en stock');
                            return $this->redirectToRoute('stock_index');
                        }
                    }
                }

                $stock->setRefStock($num);
                $stock->setCreatedAt(new \DateTime($request->request->get('date')));
                $stock->setOperation($request->request->get('operation'));

                if ($lignestockRepository->findOneBy(['article' => $stock->getArticles()->getDesignation(), 'depot' => $stock->getDepots()->getId()]) != null) {
                    $sreelF2 =  $lignestockRepository->findOneBy(['article' => $stock->getArticles()->getDesignation(), 'depot' => $stock->getDepots()->getId()], ['id' => 'DESC'])->getQtedispo() -  $request->request->get('quantite');

                    $lignestock->setQtedispo($sreelF2);
                } else {
                    $lignestock->setQtedispo(0 - $request->request->get('quantite'));
                }
                $lignestock->setQuantite($request->request->get('quantite'));
                $lignestock->setStocks($stock);
                $lignestock->setDepot($stock->getDepots()->getId());
                $lignestock->setTypeoperation($stock->getOperation());
                $lignestock->setArticle($stock->getArticles()->getDesignation());

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($stock);
                $entityManager->persist($lignestock);
                $entityManager->flush();

                return $this->redirectToRoute('stock_index');
            }

            if ($type == "LOT") {

                $num = 'MVS' . sprintf("%'.012d", $stockRepository->findOneBy([], ['id' => 'DESC'])  ? ($stockRepository->findOneBy([], ['id' => 'DESC']))->getId() + 1 : 1);
                $old = $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))]);
                if ($old == NULL) {

                    $this->addFlash('error', 'Impossible d\' éffectuer cette opération, vous devez faire une mise en stock');
                    return $this->redirectToRoute('stock_index');
                } else {
                    $stock->setArticles($article);
                    $stock->setDepots($depotRepository->find($request->request->get('depot')));


                    /*  if ($stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC']) == null) {
                    $sortie = 0 - $request->request->get('quantite');

                    if ($stock->setStockReel($sortie)->getStockReel()  < 0) {
                        $this->addFlash('error', 'Impossible d\' éffectuer cette opération il vous reste 0 en stock');
                        return $this->redirectToRoute('stock_index');
                    }
                } else {
                    $sreelF =  $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel() -  $request->request->get('quantite');
                    if ($article->getIsStocknegatif() == true) {
                        $stock->setStockReel($sreelF);
                    } else {
                        if ($stock->setStockReel($sreelF)->getStockReel() < 0) {
                            $this->addFlash('error', 'Impossible d\' éffectuer cette opération il vous reste ' . $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel() . ' en stock');
                            return $this->redirectToRoute('stock_index');
                        }
                    }
                } */
                    $stock->setRefStock($num);
                    // $stock->setStockReel($old->getStockReel());
                    $stock->setCreatedAt(new \DateTime($request->request->get('date')));
                    $stock->setOperation($request->request->get('operation'));
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($stock);
                    $entityManager->flush();
                }

                return $this->redirectToRoute('sortielot', [
                    'id' => $stock->getId()
                ]);
            }

            if ($type == "SERIE") {
                $num = 'MVS' . sprintf("%'.012d", $stockRepository->findOneBy([], ['id' => 'DESC'])  ? ($stockRepository->findOneBy([], ['id' => 'DESC']))->getId() + 1 : 1);
                $old = $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))]);
                // dd($old);
                if ($old == NULL) {
                    $this->addFlash('error', 'Impossible d\' éffectuer cette opération, vous devez faire une mise en stock');
                    return $this->redirectToRoute('stock_index');
                } else {
                    $stock->setArticles($article);
                    $stock->setDepots($depotRepository->find($request->request->get('depot')));
                    /*  $stock->setQuantite($request->request->get('quantite'));
                    if ($stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC']) == null) {
                        $sortie = 0 - $request->request->get('quantite');
                        if ($stock->setStockReel($sortie)->getStockReel()  < 0) {
                            $this->addFlash('error', 'Impossible d\' éffectuer cette opération il vous reste 0 en stock');
                            return $this->redirectToRoute('stock_index');
                        }
                    } else {
                        $sreelF =  $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel() -  $request->request->get('quantite');
                        if ($article->getIsStocknegatif() == true) {
                            $stock->setStockReel($sreelF);
                        } else {
                            if ($stock->setStockReel($sreelF)->getStockReel() < 0) {
                                $this->addFlash('error', 'Impossible d\' éffectuer cette opération il vous reste ' . $stockRepository->findOneBy(['articles' => $article, 'depots' => $depotRepository->find($request->request->get('depot'))], ['id' => 'DESC'])->getStockReel() . ' en stock');
                                return $this->redirectToRoute('stock_index');
                            }
                        }
                    } */
                    $stock->setRefStock($num);
                    $stock->setCreatedAt(new \DateTime($request->request->get('date')));
                    $stock->setOperation($request->request->get('operation'));
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($stock);
                    $entityManager->flush();
                }

                return $this->redirectToRoute('sortieserie', [
                    'id' => $stock->getId()
                ]);
            }
        }

        if ($request->request->get('operation') == 3) {
            $num = 'MVT' . sprintf("%'.012d", $stockRepository->findOneBy([], ['id' => 'DESC'])  ? ($stockRepository->findOneBy([], ['id' => 'DESC']))->getId() + 1 : 1);
            $stock->setRefStock($num);
            $stock->setCreatedAt(new \DateTime($request->request->get('date')));
            $stock->setDepots($depotRepository->find($request->request->get('depot')));
            $stock->setDepotrecepteur($depotRepository->find($request->request->get('depotrecepteur')));
            $stock->setOperation($request->request->get('operation'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stock);
            $entityManager->flush();
            //dd($stock);
            return $this->redirectToRoute('entreetranfert', [
                'id' => $stock->getId()
            ]);
            //dd($stock);
        }

        return $this->render('stock/index.html.twig');
    }

    /**
     * @Route("/entreetranfert/{id}", name="entreetranfert", methods={"GET","POST"})
     */
    public function entreetranfert(Stock $stock, Request $request, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository)
    {
        $valider = $request->request->get('valider');
        $tampon = null;

        if (isset($valider)) {
            $tampon = new Tampon;
            $tampon->setQtedispo($request->request->get('quantite'));
            $tampon->setArticle($request->request->get('article'));
            $tampon->setStocks($stock);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tampon);
            $entityManager->flush();
        }

        /*return $this->redirectToRoute('entreetranfert', [
            'id' => $stock->getId(),
        ]);*/

        return $this->render('stock/entreetranfert.html.twig', [
            'stock' => $stock,
            'tampons' => $tamponRepository->findAll(),
            'emmetteurs' => $stock->getDepots()->getIntitule(),
            'emmetteurid' => $stock->getDepots()->getId(),
            'recepteurs' => $stock->getDepotrecepteur()->getIntitule(),
            'recepteurid' => $stock->getDepotrecepteur()->getId(),
            'articles' => $lignestockRepository->listarticle(),
        ]);
    }

    /**
     * @Route("/validertranfert/{id}/{emmetteurid}/{recepteurid}", name="validertranfert", methods={"GET","POST"})
     */
    public function validertranfert($emmetteurid, $recepteurid, DepotRepository $depotRepository,  ArticleRepository $articleRepository, Stock $stock, Request $request, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository)
    {
        $listes =  $tamponRepository->findAll();
        $total = 0;
        foreach ($listes as $list) {
            //dd($list);
            $typesuivi = $articleRepository->findOneBy(['designation' => $list->getArticle()]);

            if ($typesuivi->getTypesuivis()->getIntitule() == "CMUP") {
                $article = $lignestockRepository->findOneBy([
                    'article' => $list->getArticle(),
                    'depot' => $emmetteurid
                ], ['id' => 'DESC']);
                $article->SetQtedispo($article->getQtedispo() - $list->getQtedispo());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($article);
                $entityManager->flush();

                $lignestock = new Lignestock();
                $lignestock->setQuantite($list->getQtedispo());
                $lignestock->setQtedispo($list->getQtedispo());
                $articleold = $lignestockRepository->findOneBy(['article' => $list->getArticle(), 'depot' => $recepteurid], ['id' => 'DESC']);
                if ($articleold) {
                    $lignestock->setQtedispo($articleold->getQtedispo() + $list->getQtedispo());
                }
                $lignestock->setStocks($stock);
                $lignestock->setDepot($recepteurid);
                $lignestock->setDepotrecepteurs($depotRepository->find($emmetteurid));
                $lignestock->setTypeoperation($stock->getOperation());
                $lignestock->setArticle($list->getArticle());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($lignestock);
                $entityManager->flush();
            }

            if ($typesuivi->getTypesuivis()->getIntitule() == "SERIE") {
               
                $listseries = $lignestockRepository->findBy(['article' => $list->getArticle(), 
                'depot' => $emmetteurid, 'qtedispo' => '1']);
                $nbre = 0;
                foreach ($listseries as $value) {
                    $nbre++;
                    if ($nbre > $list->getQtedispo()) {
                    } else {
                        $value->setQtedispo(0);
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($value);
                        $entityManager->flush();

                        $lignestock = new Lignestock();
                        $lignestock->setQuantite(1);
                        $lignestock->setQtedispo(1);
                        $lignestock->setStocks($stock);
                        $lignestock->setDepot($recepteurid);
                        $lignestock->setDepotrecepteurs($depotRepository->find($emmetteurid));
                        $lignestock->setTypeoperation($stock->getOperation());
                        $lignestock->setArticle($value->getArticle());
                        $lignestock->setNserie($value->getNserie());
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($lignestock);
                        $entityManager->flush();
                    }
                }
            }

            if ($typesuivi->getTypesuivis()->getIntitule() == "LOT") {
                //$listlots = $lignestockRepository->findBy(['article' => $list->getArticle(), 'depot' => $emmetteurid]);
                 //dd($listlots);
                $listlots = $lignestockRepository->listlot($list->getArticle(), $emmetteurid);
            
               // dd($listlots);
                foreach ($listlots as  $value) {
                    $nbre = $value->getQtedispo() - $list->getQtedispo();
                    dd($nbre);
                    if($nbre <= 0 ){
                        $value->SetQtedispo(0);
                    }
                    else{
                        $value->SetQtedispo($value->getQtedispo() - $list->getQtedispo());
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($value);
                        $entityManager->flush();
                    } 
                }
            }
            $total = $list->getQtedispo();
        }
        $tamponRepository->delTamp();
        $stock->setQuantite($total);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($stock);
        $entityManager->flush();
        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/entreeserie/{id}", name="entreeserie", methods={"GET","POST"})
     */
    public function entreeserie(Stock $stock, Request $request, StockRepository $stockRepository, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository)
    {
        $auto = $request->request->get('auto');
        $debut = $request->request->get('debut');
        $fin = $request->request->get('fin');
        $prefixe = $request->request->get('prefixe');

        $art = $stockRepository->findOneBy(['id' => $stock]);
        $nserie = $request->request->get('nserie');
        $sumquantite = $tamponRepository->sumQantite();
        $reste  = $stock->getQuantite()  - $sumquantite[0]['quantite'];
        $existant =  $tamponRepository->findOneBy(['nserie' =>  $nserie]);
        $existant2 =  $lignestockRepository->findOneBy(['article' => $stock->getArticles()->getDesignation(), 'nserie' =>  $nserie]);
        $series = [];
        $tab = null;

        //generate entree automatique
        if (isset($auto)) {
            // preg_match_all('!\d+!', $premiereserie, $matches);
            // dd($matches,$qte);

            for ($i = $debut; $i <= $fin; $i++) {
                $series[] = $prefixe . sprintf("%'.012d", $i);
            }

            foreach ($series as  $nserie) {

                $existant =  $tamponRepository->findOneBy(['nserie' =>  $nserie]);
                $existant2 =  $lignestockRepository->findOneBy(['article' => $stock->getArticles()->getDesignation(), 'nserie' =>  $nserie]);

                if ($existant  and $existant->getNserie() == $nserie) {
                    $this->addFlash('error', 'Le numéro de série ' . $nserie . ' existe déjà! ');
                    return $this->redirectToRoute('entreeserie', [
                        'id' => $stock->getId(),
                    ]);
                } elseif ($existant2 and $existant2->getNserie() == $nserie) {
                    $this->addFlash('error', 'Le numéro de série ' . $nserie . ' existe déjà! ');
                    return $this->redirectToRoute('entreeserie', [
                        'id' => $stock->getId(),
                    ]);
                } else {
                    $tampon = new Tampon;
                    $tampon->setNserie($nserie);
                    $tampon->setQuantite(1);
                    $tampon->setStocks($stock);
                    $tampon->setArticle($stock->getArticles()->getDesignation());
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($tampon);
                    $entityManager->flush();
                }
            }

            return $this->redirectToRoute('entreeserie', [
                'id' => $stock->getId(),
            ]);
        }

        //generate entree manuelle
        if ($nserie != null) {

            if ($existant or $existant2) {
                $this->addFlash('error', 'Le numéro de série ' . $nserie . ' existe déjà! ');
                return $this->redirectToRoute('entreeserie', [
                    'id' => $stock->getId(),
                ]);
            } else {

                $tampon = new Tampon;
                $tampon->setNserie($nserie);
                $tampon->setQuantite(1);
                $tampon->setStocks($stock);
                $tampon->setArticle($stock->getArticles()->getDesignation());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($tampon);
                $entityManager->flush();
                return $this->redirectToRoute('entreeserie', [
                    'id' => $stock->getId(),
                ]);
            }
        }

        return $this->render('stock/entreeserie.html.twig', [
            'stock' => $stock,
            'article' => $art->getArticles()->getDesignation(),
            'tampons' =>  $tamponRepository->findBy(['stocks' => $stock]),
            'reste' => $reste

        ]);
    }

    /**
     * @Route("/detailentree/{id}", name="detailentree")
     */
    public function detailentree(Stock $stock, LignestockRepository $lignestockRepository)
    {
        return $this->render('stock/detailentree.html.twig', [
            'details' => $lignestockRepository->findBy(['stocks' => $stock]),
            'type' => $lignestockRepository->findOneBy(['stocks' => $stock])
        ]);
    }

    /**
     * @Route("/validateserie/{id}", name="validateserie", methods={"GET","POST"})
     */
    public function validateserie(Stock $stock, TamponRepository $tamponRepository, EntityManagerInterface $em)
    {
        $lignes = $tamponRepository->findAll();
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            foreach ($lignes as  $ligne) {
                $lignestock = new Lignestock();
                $lignestock->setNserie($ligne->getNserie());
                $lignestock->setQuantite($ligne->getQuantite());
                $lignestock->setQtedispo($ligne->getQuantite());
                $lignestock->setStocks($stock);
                $lignestock->setDepot($stock->getDepots()->getId());
                $lignestock->setTypeoperation($stock->getOperation());
                $lignestock->setArticle($stock->getArticles()->getDesignation());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($lignestock);
                $entityManager->flush();
            }

            //suppression des élements de la table tampon
            $tamponRepository->delTamp();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/entreelot/{id}", name="entreelot", methods={"GET","POST"})
     */
    public function entree(Stock $stock, Request $request, StockRepository $stockRepository, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository)
    {

        $art = $stockRepository->findOneBy(['id' => $stock]);
        $nlot = $request->request->get('nlot');
        $quantite = $request->request->get('quantite');
        $prix = $request->request->get('prix');
        $sumquantite = $tamponRepository->sumQantite();
        $reste  = $stock->getQuantite()  - $sumquantite[0]['quantite'];
        $existant =  $tamponRepository->findBy(['nlot' =>  $nlot]);
        $existant2 =  $lignestockRepository->findOneBy(['article' => $stock->getArticles()->getDesignation(), 'nlot' =>  $nlot]);

        if ($nlot != null && $quantite != null) {

            if ($existant or $existant2) {
                $this->addFlash('error', 'Ce numéro de lot existe déjà! ');
                return $this->redirectToRoute('entreelot', [
                    'id' => $stock->getId(),
                ]);
            }
            $tampon = new Tampon;
            $tampon->setNlot($nlot);
            $tampon->setQuantite($quantite);
            $tampon->setStocks($stock);
            $tampon->setPrixUnitaire($prix);
            $tampon->setArticle($stock->getArticles()->getDesignation());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tampon);
            $entityManager->flush();

            return $this->redirectToRoute('entreelot', [
                'id' => $stock->getId(),
            ]);
        }
        return $this->render('stock/entreelot.html.twig', [
            'stock' => $stock,
            'article' => $art->getArticles()->getDesignation(),
            'reste' => $reste,
            'tampons' =>  $tamponRepository->findBy(['stocks' => $stock])

        ]);
    }

    /**
     * @Route("/sortielot/{id}", name="sortielot", methods={"GET","POST"})
     */
    public function sortielot(Stock $stock, Request $request, StockRepository $stockRepository, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository)
    {

        // dd( $lignestockRepository->infoLot($stock->getArticles()->getDesignation(), $stock->getDepots()->getId()));
        $nlot = $request->request->get('nlot');
        $quantite = $request->request->get('quantite');

        if ($nlot != null && $quantite != null) {

            $old = $tamponRepository->findOneBy(['nlot' => $nlot]);

            if ($old) {
                $this->addFlash('error', 'Impossible d\' éffectuer cette opération sur ce lot, Faites une mise à jour');
                return $this->redirectToRoute('sortielot', [
                    'id' => $stock->getId(),
                ]);
            } else {

                $tampon = new Tampon;
                $tampon->setNlot($nlot);
                $tampon->setQuantite($quantite);
                $tampon->setStocks($stock);
                $tampon->setArticle($stock->getArticles()->getDesignation());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($tampon);
                $entityManager->flush();

                return $this->redirectToRoute('sortielot', [
                    'id' => $stock->getId(),
                ]);
            }
        }
        return $this->render('stock/sortielot.html.twig', [
            //'lots' =>  $lignestockRepository->findBy(['article' => $stock->getArticles()->getDesignation(), 'depot' => $stock->getDepots()->getId()]),
            'lots' => $lignestockRepository->infoLot($stock->getArticles()->getDesignation(), $stock->getDepots()->getId()),
            'libelle' => $stock->getArticles()->getDesignation(),
            'stock' => $stock,
            'tampons' =>  $tamponRepository->findBy(['stocks' => $stock])
        ]);
    }

    /**
     * @Route("/sortieserie/{id}", name="sortieserie", methods={"GET","POST"})
     */
    public function sortieserie(Request $request, Stock $stock, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository)
    {
        $nserie = $request->request->get('nserie');
        $old = $tamponRepository->findBy(['nserie' => $nserie]);

        if ($nserie != null) {
            if ($old) {
                $this->addFlash('error', 'Ce numéro de série a été déjà sélectionner ');
                return $this->redirectToRoute('sortieserie', [
                    'id' => $stock->getId(),
                ]);
            } else {
                $tampon = new Tampon;
                $tampon->setNserie($nserie);
                $tampon->setQuantite(1);
                $tampon->setStocks($stock);
                $tampon->setArticle($stock->getArticles()->getDesignation());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($tampon);
                $entityManager->flush();

                return $this->redirectToRoute('sortieserie', [
                    'id' => $stock->getId(),
                ]);
            }
        }
        return $this->render('stock/sortieserie.html.twig', [
            'series' =>  $lignestockRepository->infoSerie($stock->getArticles()->getDesignation(), $stock->getDepots()->getId()),
            'art' => $stock->getArticles()->getDesignation(),
            'stock' => $stock,
            'tampons' =>  $tamponRepository->findBy(['stocks' => $stock])
        ]);
    }

    /**
     * @Route("/validatelot/{id}", name="validatelot", methods={"GET","POST"})
     */
    public function validatelot(Stock $stock, TamponRepository $tamponRepository, EntityManagerInterface $em)
    {

        $lignes = $tamponRepository->findAll();
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            foreach ($lignes as  $ligne) {
                $lignestock = new Lignestock();
                $lignestock->setNlot($ligne->getNlot());
                if ($stock->getOperation() == 1) {
                    $lignestock->setQtedispo($ligne->getQuantite());
                }
                $lignestock->setQuantite($ligne->getQuantite());
                $lignestock->setStocks($stock);
                $lignestock->setDepot($stock->getDepots()->getId());
                $lignestock->setPrixUnitaire($ligne->getPrixUnitaire());
                $lignestock->setTypeoperation($stock->getOperation());
                $lignestock->setArticle($stock->getArticles()->getDesignation());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($lignestock);
                $entityManager->flush();
            }

            //suppression des élements de la table tampon
            $tamponRepository->delTamp();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/validatesortielot/{id}", name="validatesortielot", methods={"GET","POST"})
     */
    public function validatesortielot(Stock $stock, StockRepository $stockRepository, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository, EntityManagerInterface $em)
    {

        $lignes = $tamponRepository->findAll();
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            $total = 0;
            foreach ($lignes as  $ligne) {
                $old = $lignestockRepository->findOneBy(['nlot' => $ligne->getNlot(), 
                'depot' => $stock->getDepots()->getId(), 
                'article' => $stock->getArticles()->getDesignation()], ['id' => 'DESC']);

                $lignestock = new Lignestock();
                $lignestock->setNlot($ligne->getNlot());
                if ($stock->getOperation() == 1) {
                    $lignestock->setQtedispo($ligne->getQuantite());
                } else {
                    $lignestock->setQtedispo($old->getQtedispo() - $ligne->getQuantite());
                }
                $lignestock->setQuantite($ligne->getQuantite());
                $lignestock->setStocks($stock);
                $lignestock->setDepot($stock->getDepots()->getId());
                $lignestock->setPrixUnitaire($ligne->getPrixUnitaire());
                $lignestock->setTypeoperation($stock->getOperation());
                $lignestock->setArticle($stock->getArticles()->getDesignation());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($lignestock);
                $entityManager->flush();

                $total += $ligne->getQuantite();
            }

            $oldstock = $stockRepository->findOneBy([], ['id' => 'DESC']);
            $firstligne = $stockRepository->findBy(['articles' => $stock->getArticles(), 'depots' => $stock->getDepots()], ['id' => 'DESC'], 2);
            $sumQteSortie = $lignestockRepository->sumQteSortie($oldstock->getId());
            $stock->setQuantite($total);
            $stock->setStockReel($firstligne[1]->getStockReel() - $sumQteSortie[0]['quantite']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stock);
            $entityManager->flush();

            //suppression des élements de la table tampon
            $tamponRepository->delTamp();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/validatesortieserie/{id}", name="validatesortieserie", methods={"GET","POST"})
     */
    public function validatesortieserie(Stock $stock, StockRepository $stockRepository, TamponRepository $tamponRepository, LignestockRepository $lignestockRepository, EntityManagerInterface $em)
    {

        $lignes = $tamponRepository->findAll();

        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            $total = 0;
            foreach ($lignes as  $ligne) {
                //dd($ligne);
                $old = $lignestockRepository->findOneBy([
                    'nserie' => $ligne->getNserie(),
                    'depot' => $stock->getDepots()->getId(),
                    'article' => $stock->getArticles()->getDesignation()
                ], ['id' => 'DESC']);

                $old->setQtedispo(0);

                /*  $lignestock = new Lignestock();
                $lignestock->setNserie($ligne->getNserie());
                if ($stock->getOperation() == 2) {
                    $lignestock->setQtedispo($old->getQtedispo() - $ligne->getQuantite());
                }
                $lignestock->setQuantite($ligne->getQuantite());
                $lignestock->setStocks($stock);
                $lignestock->setDepot($stock->getDepots()->getId());
                $lignestock->setPrixUnitaire($ligne->getPrixUnitaire());
                $lignestock->setTypeoperation($stock->getOperation());
                $lignestock->setArticle($stock->getArticles()->getDesignation()); */
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($old);
                $entityManager->flush();

                $total += $ligne->getQuantite();
            }

            $oldstock = $stockRepository->findOneBy([], ['id' => 'DESC']);
            $firstligne = $stockRepository->findBy(['articles' => $stock->getArticles(), 'depots' => $stock->getDepots()], ['id' => 'DESC'], 2);
            $sumQteSortie = $lignestockRepository->sumQteSortie($oldstock->getId());
            $stock->setQuantite($total);
            $stock->setStockReel($firstligne[1]->getStockReel() - $sumQteSortie[0]['quantite']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stock);
            $entityManager->flush();



            //suppression des élements de la table tampon
            $tamponRepository->delTamp();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/delentree/{id}", name="delentree", methods={"GET","POST"})
     */
    public function delentree(Stock $stock, TamponRepository $tamponRepository)
    {
        $tamponRepository->delTamp();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($stock);
        $entityManager->flush();
        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/annulersortie/{id}", name="annulersortie", methods={"GET","POST"})
     */
    public function annulersortie(Stock $stock, TamponRepository $tamponRepository)
    {
        $tamponRepository->delTamp();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($stock);
        $entityManager->flush();
        return $this->redirectToRoute('stock_index');
    }
    /**
     * @Route("/annulertransfert/{id}", name="annulertransfert", methods={"GET","POST"})
     */
    public function annulertransfert(Stock $stock, TamponRepository $tamponRepository)
    {
        $tamponRepository->delTamp();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($stock);
        $entityManager->flush();
        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/tampondelete/{id}", name="tampondelete")
     */
    public function tampondelete(Tampon $tampon)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tampon);
        $entityManager->flush();


        return $this->redirectToRoute('entreelot', [
            'id' => $tampon->getStocks()->getId(),
        ]);
    }

    /**
     * @Route("/tampondeletesortielot/{id}", name="tampondeletesortielot")
     */
    public function tampondeletesortielot(Tampon $tampon)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tampon);
        $entityManager->flush();
        return $this->redirectToRoute('sortielot', [
            'id' => $tampon->getStocks()->getId(),
        ]);
    }


    /**
     * @Route("/tampondeleteserie/{id}", name="tampondeleteserie")
     */
    public function tampondeleteserie(Tampon $tampon)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tampon);
        $entityManager->flush();

        return $this->redirectToRoute('entreeserie', [
            'id' => $tampon->getStocks()->getId(),
        ]);
    }
    /**
     * @Route("/tampondeletetransfert/{id}", name="tampondeletetransfert")
     */
    public function tampondeletetransfert(Tampon $tampon)
    {
        // dd($tampon);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tampon);
        $entityManager->flush();

        return $this->redirectToRoute('entreetranfert', [
            'id' => $tampon->getStocks()->getId(),
        ]);
    }

    /**
     * @Route("/tampondeletesortie/{id}", name="tampondeletesortie")
     */
    public function tampondeletesortie(Tampon $tampon)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tampon);
        $entityManager->flush();

        return $this->redirectToRoute('sortieserie', [
            'id' => $tampon->getStocks()->getId(),
        ]);
    }

    /**
     * @Route("/sortie", name="sortie", methods={"GET","POST"})
     */
    public function sortie(Request $request, ArticleRepository $articleRepository)
    {
        return $this->render('stock/sortie.html.twig');
    }

    /**
     * @Route("/{id}", name="stock_show", methods={"GET"})
     */
    public function show(Stock $stock): Response
    {
        return $this->render('stock/show.html.twig', [
            'stock' => $stock,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="stock_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Stock $stock): Response
    {
        $form = $this->createForm(StockType::class, $stock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('stock_index');
        }

        return $this->render('stock/edit.html.twig', [
            'stock' => $stock,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="stock_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Stock $stock): Response
    {
        if ($this->isCsrfTokenValid('delete' . $stock->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($stock);
            $entityManager->flush();
        }

        return $this->redirectToRoute('stock_index');
    }

    /**
     * @Route("/search", name="searchtype", methods={"GET","POST"})
     */
    public function searchtype(Request $request, ArticleRepository $articleRepository)
    {
        $exist = $articleRepository->findOneBy(['id' => $request->request->get('id')]);
        $data =  $exist->getTypesuivis()->getIntitule();
        return new JsonResponse(['data' => $data]);
    }
}
