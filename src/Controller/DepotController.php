<?php

namespace App\Controller;

use App\Entity\Depot;
use App\Form\DepotType;
use App\Repository\DepotRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/depot")
 */
class DepotController extends AbstractController
{
    /**
     * @Route("/", name="depot_index", methods={"GET"})
     */
    public function index(DepotRepository $depotRepository): Response
    {
        return $this->render('depot/index.html.twig', [
            'depots' => $depotRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="depot_new", methods={"GET","POST"})
     */
    public function new(Request $request, DepotRepository $depotRepository): Response
    {
        $depot = new Depot();
        $depotexist = $depotRepository->findBy(['code'=> $request->request->get('code1')]);
        $intituleesit = $depotRepository->findBy(['intitule' => $request->request->get('intitule1')]);
        if ($depotexist or $intituleesit) {
            $this->addFlash('error', 'Le code où l\'intitule existe déjà');
            return $this->redirectToRoute('depot_index');
        }else {
            $depot->setIntitule($request->request->get('intitule1'));
            $depot->setCode($request->request->get('code1'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($depot);
            $entityManager->flush();
            $this->addFlash('success', 'Magasin crée avec success!');
            return $this->redirectToRoute('depot_index');
        }
    }

    /**
     * @Route("/{id}", name="depot_show", methods={"GET"})
     */
    public function show(Depot $depot): Response
    {
        return $this->render('depot/show.html.twig', [
            'depot' => $depot,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="depot_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Depot $depot): Response
    {

        $depot->setCode($request->request->get('code2'));
        $depot->setIntitule($request->request->get('intitule2'));
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Magasin modifié avec success!');
        return $this->redirectToRoute('depot_index');
    }

    /**
     * @Route("/{id}", name="depot_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Depot $depot): Response
    {
        $depotExist = $depot->getStocks()->toArray();
        if (sizeof($depotExist) == 0) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($depot);
            $entityManager->flush();
            $this->addFlash('success', 'suppression éffectuée avec success!');
            return $this->redirectToRoute('depot_index');
        } else {
            $this->addFlash('error', 'Impossible de supprimer, ce Magasin est déjà lié à une entrée en stock!');

            return $this->redirectToRoute('depot_index');
        }
    }
}
