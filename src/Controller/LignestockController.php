<?php

namespace App\Controller;

use App\Entity\Lignestock;
use App\Form\LignestockType;
use App\Repository\LignestockRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lignestock")
 */
class LignestockController extends AbstractController
{
    /**
     * @Route("/", name="lignestock_index", methods={"GET"})
     */
    public function index(LignestockRepository $lignestockRepository): Response
    {
        return $this->render('lignestock/index.html.twig', [
            'lignestocks' => $lignestockRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="lignestock_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $lignestock = new Lignestock();
        $form = $this->createForm(LignestockType::class, $lignestock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lignestock);
            $entityManager->flush();

            return $this->redirectToRoute('lignestock_index');
        }

        return $this->render('lignestock/new.html.twig', [
            'lignestock' => $lignestock,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lignestock_show", methods={"GET"})
     */
    public function show(Lignestock $lignestock): Response
    {
        return $this->render('lignestock/show.html.twig', [
            'lignestock' => $lignestock,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="lignestock_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Lignestock $lignestock): Response
    {
        $form = $this->createForm(LignestockType::class, $lignestock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lignestock_index');
        }

        return $this->render('lignestock/edit.html.twig', [
            'lignestock' => $lignestock,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lignestock_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lignestock $lignestock): Response
    {
        if ($this->isCsrfTokenValid('delete'.$lignestock->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lignestock);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lignestock_index');
    }
}
