<?php

namespace App\Controller;

use App\Entity\Famille;
use App\Form\FamilleType;
use App\Repository\FamilleRepository;
use Flasher\Prime\FlasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/famille")
 */
class FamilleController extends AbstractController
{
    /**
     * @Route("/", name="famille_index", methods={"GET","POST"})
     */
    public function index(Request $request, FamilleRepository $familleRepository): Response
    {
        $famille = new Famille();
        $form = $this->createForm(FamilleType::class, $famille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($famille);
            $entityManager->flush();
        }

        return $this->render('famille/index.html.twig', [
            'familles' => $familleRepository->findAll(),
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/new", name="famille_new", methods={"GET","POST"})
     */
    public function new(Request $request, FamilleRepository $familleRepository): Response
    {

        $famille = new Famille();
        $familleold = $familleRepository->findBy(['intitule'=> $request->request->get('intitule1')]);
        
        if ($familleold) {
            $this->addFlash('error', 'Cette famille existe déjà');
            return $this->redirectToRoute('famille_index');
        }else {
            $intitule = $request->request->get('intitule1');
            $famille->setIntitule($intitule);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($famille);
            $entityManager->flush();
            $this->addFlash('success', 'Famille créee avec success!');
            return $this->redirectToRoute('famille_index');
        }
    }

    /**
     * @Route("/{id}", name="famille_show", methods={"GET"})
     */
    public function show(Famille $famille): Response
    {
        return $this->render('famille/show.html.twig', [
            'famille' => $famille,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="famille_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Famille $famille): Response
    {
            
            $intitule = $request->request->get('intitule2');
            $famille->setIntitule($intitule);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Famille modifiée avec success!');
            return $this->redirectToRoute('famille_index');
    

    }

    /**
     * @Route("/{id}", name="famille_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Famille $famille): Response
    {
        $familleExist = $famille->getArticles()->toArray();

        if (sizeof($familleExist) == 0) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($famille);
            $entityManager->flush();
            $this->addFlash('success', 'suppression éffectuée avec success!');
            return $this->redirectToRoute('famille_index');
        } else {
            $this->addFlash('error', 'Impossible de supprimer.Cette famille est déjà liée à un ou plusieurs article!');

            return $this->redirectToRoute('famille_index');
        }
    }
}
