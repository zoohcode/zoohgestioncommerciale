<?php

namespace App\Controller;

use App\Entity\Categorietarifaire;
use App\Form\CategorietarifaireType;
use App\Repository\CategorietarifaireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categorietarifaire")
 */
class CategorietarifaireController extends AbstractController
{
    /**
     * @Route("/", name="categorietarifaire_index", methods={"GET"})
     */
    public function index(CategorietarifaireRepository $categorietarifaireRepository): Response
    {
        return $this->render('categorietarifaire/index.html.twig', [
            'categorietarifaires' => $categorietarifaireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="categorietarifaire_new", methods={"GET","POST"})
     */
    public function new(Request $request, CategorietarifaireRepository $categorietarifaireRepository): Response
    {
        $categorietarifaire = new Categorietarifaire();
        $categorietarifaireold = $categorietarifaireRepository->findBy(['libelle'=> $request->request->get('intitule1')]);
        if ($categorietarifaireold) {
            $this->addFlash('error', 'Cette Catégorie tarifaire existe déjà');
            return $this->redirectToRoute('categorietarifaire_index');
        } else {
            $categorietarifaire->setLibelle($request->request->get('intitule1'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorietarifaire);
            $entityManager->flush();
            $this->addFlash('success', 'Catégorie tarifaire créee avec success!');
            return $this->redirectToRoute('categorietarifaire_index');
        }
        
    }

    /**
     * @Route("/{id}", name="categorietarifaire_show", methods={"GET"})
     */
    public function show(Categorietarifaire $categorietarifaire): Response
    {
        return $this->render('categorietarifaire/show.html.twig', [
            'categorietarifaire' => $categorietarifaire,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="categorietarifaire_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Categorietarifaire $categorietarifaire): Response
    {
        $categorietarifaire->setLibelle($request->request->get('intitule2'));
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Catégorie tarifaire modifiée avec success!');
        return $this->redirectToRoute('categorietarifaire_index');
    }

    /**
     * @Route("/{id}", name="categorietarifaire_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Categorietarifaire $categorietarifaire): Response
    {
        $clientExist = $categorietarifaire->getTiers()->toArray();
        if (sizeof($clientExist) == 0) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categorietarifaire);
            $entityManager->flush();
            $this->addFlash('success', 'suppression éffectuée avec success!');
            return $this->redirectToRoute('categorietarifaire_index');
        } else {
            $this->addFlash('error', 'Impossible de supprimer, cette catégorie tarifaire est déjà liée à un client!');

            return $this->redirectToRoute('categorietarifaire_index');
        }
    }
}
