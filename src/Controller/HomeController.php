<?php

namespace App\Controller;

use App\Entity\Depot;
use App\Entity\Facture;
use App\Entity\Societe;
use App\Entity\Reglement;
use App\Entity\Lignefacture;
use App\Service\FileUploader;
use App\Entity\Tamponlignefacture;
use App\Repository\DepotRepository;
use App\Repository\TarifRepository;
use App\Repository\TiersRepository;
use App\Repository\ArticleRepository;
use App\Repository\FactureRepository;
use App\Repository\SocieteRepository;
use App\Repository\ReglementRepository;
use App\Repository\TypefactureRepository;
use App\Repository\LignefactureRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategorietarifaireRepository;
use App\Repository\LignestockRepository;
use App\Repository\TamponlignefactureRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, FactureRepository $factureRepository, LignefactureRepository $lignefactureRepository, TiersRepository $tiersRepository, TypefactureRepository $typefactureRepository): Response
    { 
        $valider = $request->request->get('valider');
        $facture = new Facture();
        $typefacture = $request->request->get('typefacture');
        $client = $request->request->get('client');

        if (isset($valider)) {
            $typefacture = $typefactureRepository->find($typefacture);
            $client = $tiersRepository->findBy(['code' => $client]);
            $num = $factureRepository->findOneBy([], ['id' => 'DESC']);
            $num == null ? $num == 1 : $num =  (($num->getId()) + 1);
            $num = 'Fa' . sprintf("%'.012d", $num);

            $facture->setNumeroFacture($num);
            $facture->setTypefactures($typefacture);
            if ($typefacture->getId() == 1 ) {
                $facture->setIsPayer(true);
            }else {
                $facture->setIsPayer(false);
            }
           
            $facture->setClients($client[0]);
            $facture->setDateFacture(new \DateTime("now"));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($facture);
            $entityManager->flush();

            return $this->redirectToRoute('detailfacture', [
                'id' => $facture->getId()
            ]);
        }
        return $this->render('home/index.html.twig', [
            'typefactures' => $typefactureRepository->findAll(),
            'factures' => $factureRepository->findBy([], ['id' => 'DESC']),
            'tiers' => $tiersRepository->findAll(),
        ]);
    }

    /**
     * @Route("/zoomfacture/{id}", name="zoomfacture", methods={"GET","POST"})
     */
    public function zommfacture(Facture $facture, LignefactureRepository $lignefactureRepository)
    {
        $datas = [];
        $detailfactures = $lignefactureRepository->findBy(['factures' => $facture]);
       //dd($detailfactures);
        foreach ($detailfactures as $value) {
            $datas [] = ['datefacture'=>$value->getFactures()->getDateFacture(),'articles'=>$value->getArticles()->getDesignation(), 'quantite'=>$value->getQuantite(),'prixunitaire'=>$value->getPrixunitaire(), 'montant'=>$value->getMontant()];
        }
        //dd($data);
        return $this->render('home/zoomfacture.html.twig', [
            'details' => $datas,
            'factures'=> $facture,
           
        ]);
    }

    /**
     * @Route("/findtarif", name="findtarif")
     */
    public function findtarif(LignestockRepository $lignestockRepository,Request $request,TarifRepository $tarifRepository,TiersRepository $tiersRepository,ArticleRepository $articleRepository)
    {
       
        $article = $articleRepository->findOneBy(['reference' => $request->request->get('code')]);
        $data =  $article->getPrixventettc();
        if ($article->getTypesuivis()->getIntitule() == "SERIE") {
            $exist = $lignestockRepository->articleserie( $article->getDesignation(), $request->request->get('depot'));
            $qte =  $exist != null ? $exist[0]['article'] : '0';
            return new JsonResponse(['qte'=>$qte, 'data'=> $data]);
        } 

        if ($article->getTypesuivis()->getIntitule() == "CMUP") {
            $exist = $lignestockRepository->articlecmup( $article->getDesignation(), $request->request->get('depot'));
            $qte =  $exist != null ? $exist[0]['qtedispo'] : '0';
            return new JsonResponse(['qte' => $qte, 'data' => $data]);
        } 

        if ($article->getTypesuivis()->getIntitule() == "LOT") {
            $exist = $lignestockRepository->articlelot( $article->getDesignation(), $request->request->get('depot'));
            $qte =  $exist != null ? $exist[0]['qte'] : '0';
            return new JsonResponse(['qte' => $qte, 'data' => $data]);
        } 
    }

    /**
     * @Route("/reglement", name="reglement")
     */
    public function reglement(Request $request, FactureRepository $factureRepository,ReglementRepository $reglementRepository)
    {
        $listes = $factureRepository->findBy(['isPayer' => false]);
        $datas = [];
        $max = null;
        $restant = null;
        foreach ($listes as  $value) {
            $reglement = $reglementRepository->sumMontant($value->getId());
            $restant =  $value->getPrixttc() -  $reglement[0]['montant'];
          
            $datas[] = ['id' => $value->getId(),'date' =>  $value->getDateFacture(),'numerofacture' =>  $value->getNumeroFacture(),
            'client' =>  $value->getClients()->getDesignation(),'montantfacture' =>  $value->getPrixttc(),
            'montantpaye' =>  $reglement[0]['montant'], 'montantrestant' => $restant];
        }
       $max = $restant;
       //dd($max);
        return $this->render('home/reglement.html.twig', [
            'nonSolders' => $datas,
            'max' => $max,
        ]);
    }

    /**
     * @Route("/reglement/{id}", name="addreglement" ,methods={"GET","POST"})
     */
    public function addreglement(Facture $facture, Request $request,ReglementRepository $reglementRepository)
    {
        $reglement = new Reglement();
        $reglement->setFactures($facture);
        $reglement->setMontant( $request->request->get('montant'));
        $reglement->setCreatedAt(new \DateTime("now"));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reglement);
        $entityManager->flush();

        $montantregle = $reglementRepository->sumMontant($facture->getId());
     
        if ($facture->getPrixttc() == $montantregle[0]['montant'] ) {
            $facture->setIsPayer(true);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($facture);
            $entityManager->flush();
        }
        return $this->redirectToRoute('reglement');

    }
    /**
     * @Route("/detailreglement/{id}", name="detailreglement" ,methods={"GET","POST"})
     */
    public function detailreglement(Facture $facture,ReglementRepository $reglementRepository)
    {
        return $this->render('home/detailreglement.html.twig', [
            'details' => $reglementRepository->findBy(['factures' => $facture]),
            'client' => $facture->getClients()->getDesignation(),
            'fact' => $facture->getNumeroFacture()
        ]);

    }

    /**
     * @Route("/validatefacture", name="validatefacture",methods={"GET","POST"})
     */
    public function validatefacture(LignestockRepository $lignestockRepository, ArticleRepository $articleRepository, TamponlignefactureRepository $tamponlignefactureRepository, FactureRepository $factureRepository)
    {
        $alls = $tamponlignefactureRepository->findAll();
        $fact = $factureRepository->findOneBy([], ['id' => 'DESC']);
        $montantfact = 0;
        foreach ($alls as $value) {
            $typesuivi = $articleRepository->findOneBy(['designation' => $value->getArticles()->getDesignation()]);
            if ($typesuivi->getTypesuivis()->getIntitule() == "SERIE") {
                $listseries = $lignestockRepository->findBy(['article' => $value->getArticles()->getDesignation(), 
                'qtedispo' => '1', 'depot'=> $value->getDepots()->getId()]);
                $nbre = 0;
                foreach ($listseries as $valueserie) {
                    $nbre++;
                    if ($nbre > $value->getQuantite()) {
                    } else {
                        $valueserie->setQtedispo(0);
                        $valueserie->setQuantite(0);
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($valueserie);
                        $entityManager->flush();
                    }
                }
            }
            
            if ($typesuivi->getTypesuivis()->getIntitule() == "CMUP") {
                $listseries = $lignestockRepository->findOneBy([
                    'article' => $value->getArticles()->getDesignation(), 'depot'=> $value->getDepots()->getId()], ['id'=>'DESC']);
               $listseries->SetQtedispo($listseries->getQtedispo() - $value->getQuantite());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($listseries);
                $entityManager->flush();
            }

            $lignefacture = new Lignefacture();
            $lignefacture->setFactures($value->getFactures());
            $lignefacture->setArticles($value->getArticles());
            $lignefacture->setQuantite($value->getQuantite());
            $lignefacture->setPrixunitaire($value->getPrixunitaire());
            $lignefacture->setMontant($value->getMontant());
            $lignefacture->setPrixht($value->getPrixht());
            $lignefacture->setTotalht($value->getTotalht());
            $lignefacture->setTotalaib1($value->getTotalaib1());
            $lignefacture->setTotalaib2($value->getTotalaib2());
            $lignefacture->setMontantht($value->getMontantht());
            $lignefacture->setDepots($value->getDepots());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lignefacture);
            $entityManager->flush();

            $montantfact += $value->getMontant();
        }

        $fact->setPrixttc($montantfact);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($fact);
        $entityManager->flush();

        $reglement = new Reglement();
        $reglement->setFactures($fact);
        $reglement->setMontant($montantfact);
        $reglement->setCreatedAt(new \DateTime("now"));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reglement);
        $entityManager->persist($reglement);
        $entityManager->flush();

        $tamponlignefactureRepository->delTampLigne();
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/validatereglement/{id}", name="validatereglement",methods={"GET","POST"})
     */
    public function validatereglement(Facture $facture, ReglementRepository $reglementRepository)
    {
        $reglement = new Reglement();
        $facture->setIsPayer(true);
        $reglement->setFactures($facture);
        $reglement->setMontant($facture->getPrixttc());
        $reglement->setCreatedAt(new \DateTime("now"));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($facture);
        $entityManager->persist($reglement);
        $entityManager->flush();
        return $this->redirectToRoute('home');
    }

     /**
     * @Route("/validatesolde/{id}", name="validatesolde",methods={"GET","POST"})
     */
    public function validatesolde(Facture $facture, ReglementRepository $reglementRepository)
    {
        $facture->setIsPayer(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($facture);
        $entityManager->flush();
        return $this->redirectToRoute('reglement');
    }


    /**
     * @Route("/getqtedispo", name="getqtedispo")
     */
    public function getqtedispo(ArticleRepository $articleRepository, Request $request, LignestockRepository $lignestockRepository)
    {
        $type = $articleRepository->findOneBy(['designation' => $request->request->get('article')]);
      
        if ($type->getTypesuivis()->getIntitule() == "SERIE") {
            $exist = $lignestockRepository->articleserie(
                $request->request->get('article'),
                $request->request->get('depot')
            );
            $data =  $exist[0]['article'];
            return new JsonResponse(['data' => $data]);
        }
    }
    
    
    /**
     * @Route("/detailfacture/{id}", name="detailfacture", methods={"GET","POST"})
     */
    public function detailfacture(Request $request, DepotRepository $depotRepository, ArticleRepository $articleRepository, FactureRepository $factureRepository, TamponlignefactureRepository $tamponlignefactureRepository, Facture $facture)
    {
        $valider2 = $request->request->get('valider2');
        $valremise = $request->request->get('valremise');
        $tamponlignefacture = new Tamponlignefacture();
        $fact = $factureRepository->findOneBy([], ['id' => 'DESC']);
       // dd($facture->getClients()->getTaxes()->getIntitule());
        $remise = 0;
       
        if (isset($valider2)) {

            $article = $request->request->get('article');
            $prix = $request->request->get('prix');
            $quantite = $request->request->get('quantite');
            $article = $articleRepository->findBy(['reference' => $article]);
            $art = $article[0];
            $montant = $prix * $quantite;
            $prixht = $prix / 1.18;
            $totalht = $prixht * $quantite * 0.18;
            $totalaib1 = $prixht * $quantite * 0.01;
            $totalaib2 = $prixht * $quantite * 0.05;

            $tamponlignefacture->setFactures($fact);
            $tamponlignefacture->setArticles($art);
            $tamponlignefacture->setQuantite($quantite);
            $tamponlignefacture->setPrixunitaire($prix);
            $tamponlignefacture->setDepots($depotRepository->find($request->request->get('depot')));

            //client ht
            if ($facture->getClients()->getTaxes()->getIntitule() == "HT") {
                $tamponlignefacture->setPrixht($prix);
                $tamponlignefacture->setMontantht($montant);
                $tamponlignefacture->setTotalht(0);
                $tamponlignefacture->setTotalaib1(0);
                $tamponlignefacture->setTotalaib2(0);
               
            }

            //client ttc
            if ($facture->getClients()->getTaxes()->getIntitule() == "TTC"){
               $etatarticle = $art->getEtat();
        
                if ($etatarticle == "Taxable") {
                    $tamponlignefacture->setPrixht($prixht);
                    $tamponlignefacture->setTotalht($totalht);
                    $tamponlignefacture->setMontantht($prixht * $quantite);
                    $tamponlignefacture->setTotalaib1(0);
                    $tamponlignefacture->setTotalaib2(0);
                }else{
                    $tamponlignefacture->setPrixht($prix);
                    $tamponlignefacture->setMontantht($montant);
                    $tamponlignefacture->setTotalht(0);
                    $tamponlignefacture->setTotalaib1(0);
                    $tamponlignefacture->setTotalaib2(0);
                } 
            
            }

             //client ttc + aib 1%
             if ($facture->getClients()->getTaxes()->getIntitule() == "TTC + AIB 1%"){
                $etatarticle = $art->getEtat();
         
                 if ($etatarticle == "Taxable") {
                     $tamponlignefacture->setPrixht($prixht);
                     $tamponlignefacture->setTotalht($totalht);
                     $tamponlignefacture->setMontantht($prixht * $quantite);
                     $tamponlignefacture->setTotalaib1($totalaib1);
                     $tamponlignefacture->setTotalaib2(0);
                 }else{
                     $tamponlignefacture->setPrixht($prix);
                     $tamponlignefacture->setMontantht($montant);
                     $tamponlignefacture->setTotalht(0);
                     $tamponlignefacture->setTotalaib1(0);
                     $tamponlignefacture->setTotalaib2(0);
                 } 
             
             }

             //client ttc + aib 5%
             if ($facture->getClients()->getTaxes()->getIntitule() == "TTC + AIB 5%"){
                $etatarticle = $art->getEtat();
         
                 if ($etatarticle == "Taxable") {
                     $tamponlignefacture->setPrixht($prixht);
                     $tamponlignefacture->setTotalht($totalht);
                     $tamponlignefacture->setMontantht($prixht * $quantite);
                     $tamponlignefacture->setTotalaib1(0);
                     $tamponlignefacture->setTotalaib2($totalaib2);
                 }else{
                     $tamponlignefacture->setPrixht($prix);
                     $tamponlignefacture->setMontantht($montant);
                     $tamponlignefacture->setTotalht(0);
                     $tamponlignefacture->setTotalaib1(0);
                     $tamponlignefacture->setTotalaib2(0);
                 } 
             
             }
            $tamponlignefacture->setMontant($montant);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tamponlignefacture);
            $entityManager->flush();
        }

        if (isset($valremise)) {
            $remise = $request->request->get('remise');
            // dd($remise);
        }
        return $this->render('home/detailfacture.html.twig', [
            'facture' => $facture,
            'remise' => $remise,
            'articles' => $articleRepository->findAll(),
            'depots' => $depotRepository->findAll(),
            'tamponlignes' => $tamponlignefactureRepository->findAll(),

        ]);
    }

    /**
     * @Route("/deldetail/{id}", name="deldetail", methods={"GET","POST"})
     */
    public function delentree(Tamponlignefacture $tamponlignefacture)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tamponlignefacture);
        $entityManager->flush();
        return $this->redirectToRoute('detailfacture', [
            'id' => $tamponlignefacture->getFactures()->getId()
        ]);
    }
     /**
     * @Route("/delfact/{id}", name="delfact", methods={"GET","POST"})
     */
    public function delfact(Facture $facture, TamponlignefactureRepository $tamponlignefactureRepository)
    {

        $tamponlignefactureRepository->delTampLigne();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($facture);
        $entityManager->flush();
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/societe", name="societe",methods={"GET","POST"})
     */
    public function societeInfo(Request $request, SocieteRepository $societeRepository)
    {
        $size = sizeof($societeRepository->findAll());
        $valider = $request->request->get('valider');

        if (isset($valider)) {

            if ($size == 0) {
                $societe = new Societe();
                $societe->setNom($request->request->get('nom'));
                $societe->setAdresse($request->request->get('adresse'));
                $societe->setTelephone($request->request->get('telephone'));
                $societe->setIfu($request->request->get('ifu'));
                $societe->setRccm($request->request->get('rccm'));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($societe);
                $entityManager->flush();
            } else {
                $societe =  $societeRepository->findOneBy([]);
                $societe->setNom($request->request->get('nom'));
                $societe->setAdresse($request->request->get('adresse'));
                $societe->setTelephone($request->request->get('telephone'));
                $societe->setIfu($request->request->get('ifu'));
                $societe->setRccm($request->request->get('rccm'));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($societe);
                $entityManager->flush();
            }
        }
        return $this->render('home/societe.html.twig', [
            'size' => $size,
            'psociete' =>  $societeRepository->findOneBy([])
        ]);
    }

    /**
     * @Route("/depotchoice", name="depotchoice",methods={"GET","POST"})
     */
    public function depotchoice(DepotRepository $depotRepository)
    {
       
        return $this->render('home/depotchoice.html.twig', [
            
            'depots' =>  $depotRepository->findAll()
        ]);
    }
    /**
     * @Route("/stockbydepot/{id}", name="stockbydepot",methods={"GET","POST"})
     */
    public function stockbydepot(Depot $depot,LignestockRepository $lignestockRepository,ArticleRepository $articleRepository)
    {
        $listes = $lignestockRepository->listOfArticle();
    
        $datas = [];
        foreach ($listes as  $value) {
            $type = $articleRepository->findOneBy(['designation' => $value['article']]);
            if ($type->getTypesuivis()->getIntitule() == "CMUP") {
                $exist = $lignestockRepository->articlecmup(
                    $value['article'],
                    $depot->getId()
                );
                $qte =  $exist != null ?  $exist[0]['qtedispo'] : '0'; 
            }

            if ($type->getTypesuivis()->getIntitule() == "SERIE") {
                $exist = $lignestockRepository->articleserie(
                    $value['article'],
                    $depot->getId()
                );
                $qte =  $exist[0]['article'];  
            }

            if ($type->getTypesuivis()->getIntitule() == "LOT") {
                $exist = $lignestockRepository->articlelot(
                    $value['article'],
                    $depot->getId()
                );
             $qte =  $exist != null ?  $exist[0]['qte'] : '0';
            }
            
            $datas[] = ['article' => $value['article'], 'qte' => $qte];
        }
        return $this->render('home/stockbydepot.html.twig', [
            'datas' => $datas,
            'depot' => $depot
        ]);
    }

}
