<?php

namespace App\Controller;

use App\Entity\Tiers;
use App\Form\TiersType;
use App\Repository\TaxeRepository;
use App\Repository\TiersRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoriecomptableRepository;
use App\Repository\CategorietarifaireRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/tiers")
 */
class TiersController extends AbstractController
{
    /**
     * @Route("/", name="tiers_index", methods={"GET"})
     */
    public function index(TaxeRepository $taxeRepository, CategoriecomptableRepository $categoriecomptableRepository, TiersRepository $tiersRepository,CategorietarifaireRepository $categorietarifaireRepository): Response
    {
        return $this->render('tiers/index.html.twig', [
            'tiers' => $tiersRepository->findAll(),
            'categorietarifaires' => $categorietarifaireRepository->findAll(),
            'categoriecomptables' => $categoriecomptableRepository->findAll(),
            'taxes' => $taxeRepository->findAll()
        ]);
    }

    /**
     * @Route("validation", name="tiers_validation", methods={"GET","POST"})
     */
    public function validateArticle(Request $request, TiersRepository $tiersRepository)
    {

        $exist = $tiersRepository->findOneBy(['code' => $request->request->get('reference1')]);
        if ($exist != null) {
            return new JsonResponse(['errors' => 'cette réference existe déja!!']);
        } else {
            return new JsonResponse(['success' => '']);
        }
    }

    /**
     * @Route("/new", name="tiers_new", methods={"GET","POST"})
     */
    public function new(Request $request, TaxeRepository $taxeRepository, CategoriecomptableRepository $categoriecomptableRepository, CategorietarifaireRepository $categorietarifaireRepository): Response
    {
      
        $tier = new Tiers();
        $tier->setCode($request->request->get('reference1'));
        $tier->setCategorietarifaires($categorietarifaireRepository->find($request->request->get('categorie1')));
        $tier->setTaxes($taxeRepository->find($request->request->get('taxe1')));
        $tier->setDesignation($request->request->get('designation1'));
        $tier->setNumero($request->request->get('numero'));
        $tier->setAdresse($request->request->get('adresse'));
        $tier->setEtat($request->request->get('etat'));
        $tier->setIfu($request->request->get('ifu1'));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($tier);
        $entityManager->flush();
        $this->addFlash('success', 'Client enregistré avec success!');
        return $this->redirectToRoute('tiers_index');
    }

   

    /**
     * @Route("/{id}/edit", name="tiers_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tiers $tier, TaxeRepository $taxeRepository, CategoriecomptableRepository $categoriecomptableRepository, CategorietarifaireRepository $categorietarifaireRepository): Response
    {
        $tier->setCode($request->request->get('reference2'));
        $tier->setCategorietarifaires($categorietarifaireRepository->find($request->request->get('categorie2')));
        $tier->setTaxes($taxeRepository->find($request->request->get('taxe2')));
        $tier->setDesignation($request->request->get('designation2'));
        $tier->setNumero($request->request->get('numero2'));
        $tier->setAdresse($request->request->get('adresse2'));
        $tier->setIfu($request->request->get('ifu2'));
        $tier->setEtat($request->request->get('etat2'));

        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success', 'Client modifié avec success!');
        return $this->redirectToRoute('tiers_index');
    }

    /**
     * @Route("/{id}", name="tiers_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tiers $tier): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tier);
        $entityManager->flush();
        $this->addFlash('success', 'suppression éffectuée avec success!');
      
        return $this->redirectToRoute('tiers_index');
    }
}
