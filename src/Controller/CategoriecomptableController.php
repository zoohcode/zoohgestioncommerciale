<?php

namespace App\Controller;

use App\Entity\Categoriecomptable;
use App\Form\CategoriecomptableType;
use App\Repository\CategoriecomptableRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categoriecomptable")
 */
class CategoriecomptableController extends AbstractController
{
    /**
     * @Route("/", name="categoriecomptable_index", methods={"GET"})
     */
    public function index(CategoriecomptableRepository $categoriecomptableRepository): Response
    {
        return $this->render('categoriecomptable/index.html.twig', [
            'categoriecomptables' => $categoriecomptableRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="categoriecomptable_new", methods={"GET","POST"})
     */
    public function new(Request $request,CategoriecomptableRepository $categoriecomptableRepository): Response
    {
        $categoriecomptable = new Categoriecomptable();
        $familleold = $categoriecomptableRepository->findBy(['intitule'=> $request->request->get('intitule1')]);
        
        if ($familleold) {
            $this->addFlash('error', 'Cette famille existe déjà');
            return $this->redirectToRoute('categoriecomptable_index');
        }else {
            $intitule = $request->request->get('intitule1');
            $categoriecomptable->setIntitule($intitule);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categoriecomptable);
            $entityManager->flush();
            $this->addFlash('success', 'Catégorie comptable créee avec success!');
            return $this->redirectToRoute('categoriecomptable_index');
        }
    }

    /**
     * @Route("/{id}/edit", name="catecomptable_edit", methods={"GET","POST"})
     */
    public function editcatcomptable(Request $request, Categoriecomptable $categoriecomptable): Response
    {

        $intitule = $request->request->get('intitule2');
        $categoriecomptable->setIntitule($intitule);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Catégoriecomptable modifiée avec success!');
        return $this->redirectToRoute('categoriecomptable_index');
    }

    /**
     * @Route("/{id}", name="categoriecomptable_show", methods={"GET"})
     */
    public function show(Categoriecomptable $categoriecomptable): Response
    {
        return $this->render('categoriecomptable/show.html.twig', [
            'categoriecomptable' => $categoriecomptable,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="categoriecomptable_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Categoriecomptable $categoriecomptable): Response
    {
        $form = $this->createForm(CategoriecomptableType::class, $categoriecomptable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categoriecomptable_index');
        }

        return $this->render('categoriecomptable/edit.html.twig', [
            'categoriecomptable' => $categoriecomptable,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="categoriecomptable_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Categoriecomptable $categoriecomptable): Response
    {
       
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($categoriecomptable);
        $entityManager->flush();
        $this->addFlash('success', 'suppression éffectuée avec success!');

        return $this->redirectToRoute('categoriecomptable_index');
    }
}
