<?php

namespace App\Controller;

use App\Entity\Tarif;
use App\Form\TarifType;
use App\Repository\ArticleRepository;
use App\Repository\CategorietarifaireRepository;
use App\Repository\TarifRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarif")
 */
class TarifController extends AbstractController
{
    /**
     * @Route("/", name="tarif_index", methods={"GET"})
     */
    public function index(TarifRepository $tarifRepository,ArticleRepository $articleRepository,CategorietarifaireRepository $categorietarifaireRepository): Response
    {
        return $this->render('tarif/index.html.twig', [
            'tarifs' => $tarifRepository->findAll(),
            'articles' => $articleRepository->findAll(),
            'categorietarifaires' => $categorietarifaireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tarif_new", methods={"GET","POST"})
     */
    public function new(Request $request, TarifRepository $tarifRepository, ArticleRepository $articleRepository,CategorietarifaireRepository $categorietarifaireRepository): Response
    {
        $tarif = new Tarif();
        $prixold = $tarifRepository->findBy(['articles'=> $articleRepository->find($request->request->get('article1')), 'prix'=> $request->request->get('prix1'), 
        'categorietarifaires'=> $categorietarifaireRepository->find($request->request->get('categorie1'))]);

        if ( $prixold) {
            $this->addFlash('error', 'Ce tarif existe déjà');
            return $this->redirectToRoute('tarif_index');
        } else {
             $tarif->setCategorietarifaires($categorietarifaireRepository->find($request->request->get('categorie1')));
             $tarif->setArticles($articleRepository->find($request->request->get('article1')));
             $tarif->setPrix($request->request->get('prix1'));
             $tarif->setNatureprix($request->request->get('nature'));
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($tarif);
             $entityManager->flush();
           
             $this->addFlash('success', 'tarif enregistré avec success!');
             return $this->redirectToRoute('tarif_index');
        }
        
    }

    /**
     * @Route("/{id}", name="tarif_show", methods={"GET"})
     */
    public function show(Tarif $tarif): Response
    {
        return $this->render('tarif/show.html.twig', [
            'tarif' => $tarif,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tarif_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tarif $tarif,ArticleRepository $articleRepository,CategorietarifaireRepository $categorietarifaireRepository): Response
    {
        $tarif->setCategorietarifaires($categorietarifaireRepository->find($request->request->get('categorie2')));
        $tarif->setArticles($articleRepository->find($request->request->get('article2')));
        $tarif->setPrix($request->request->get('prix2'));
        $tarif->setNatureprix($request->request->get('nature2'));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($tarif);
        $entityManager->flush();
        $this->addFlash('success', 'tarif modifié avec success!');
        return $this->redirectToRoute('tarif_index');
    }

    /**
     * @Route("/{id}", name="tarif_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tarif $tarif): Response
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tarif);
            $entityManager->flush();
            $this->addFlash('success', 'suppression éffectuée avec success!');

        return $this->redirectToRoute('tarif_index');
    }
}
