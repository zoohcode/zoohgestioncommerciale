<?php

namespace App\Controller;

use App\Entity\Tarif;
use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\TarifRepository;
use App\Repository\ArticleRepository;
use App\Repository\FamilleRepository;
use App\Repository\TypesuiviRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ContainerHXhH1OQ\getTarifRepositoryService;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategorietarifaireRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("validation", name="article_validation", methods={"GET","POST"})
     */
    public function validateArticle(Request $request, ArticleRepository $articleRepository)
    {

        $exist = $articleRepository->findOneBy(['reference' => $request->request->get('reference1')]);
        if ($exist != null) {
            return new JsonResponse(['errors' => 'ce code existe déja!!']);
        } else {
            return new JsonResponse(['success' => '']);
        }
    }

    /**
     * @Route("/", name="article_index", methods={"GET","POST"})
     */
    public function index(ArticleRepository $articleRepository, FamilleRepository $familleRepository, TypesuiviRepository $typesuiviRepository): Response
    {
        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findBy(['statut' => false]),
            'familles' => $familleRepository->findAll(),
            'suivis' => $typesuiviRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="article_new", methods={"GET","POST"})
     */
    public function new(TarifRepository $tarifRepository, CategorietarifaireRepository $categorietarifaireRepository, Request $request, FamilleRepository $familleRepository, ArticleRepository $articleRepository, TypesuiviRepository $typesuiviRepository): Response
    {
        //dd($request->request->get('prix1'));
        $exist = $articleRepository->findOneBy(['reference' => $request->request->get('reference1')]);
        $categories = $categorietarifaireRepository->findAll();
        if ($exist != null) {
            $msg = $request->request->get('reference1');
            $this->addFlash('error', "l'enregistrement à echoué ! $msg  existe déja");
            return $this->generateUrl('article_index', [
                'articles' => $articleRepository->findBy(['statut' => false]),
                'familles' => $familleRepository->findAll(),
                'exist' => true,
                'famille' => $request->request->get('famille'),
                'reference' => $request->request->get('reference1'),
                'designation' => $request->request->get('designation1'),
                'prix' => $request->request->get('prix1'),
                'prixventeht' => $request->request->get('prixventeht1'),
                'prixventettc' => $request->request->get('prixventettc1'),
            ]);
        } else {

            $article = new Article();
            if ($request->request->get('prix1') == null) {
                $article->setIsStocknegatif(false);
            } else {
                $article->setIsStocknegatif(true);
            }
            $article->setFamilles($familleRepository->find($request->request->get('famille')));
            $article->setTypesuivis($typesuiviRepository->find($request->request->get('suivi')));
            $article->setReference($request->request->get('reference1'));
            $article->setEtat($request->request->get('etat'));
            $article->setDesignation($request->request->get('designation1'));
            $article->setPrix($request->request->get('prix1'));
            $article->setPrixventeht($request->request->get('prixventeht1'));
            $article->setPrixventettc($request->request->get('prixventettc1'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
          /*   foreach ($categories as  $value) {
                $tarif = new Tarif();
                $tarif->setCategorietarifaires($value);
                $tarif->setArticles($article);
                $tarif->setPrix($request->request->get('prix1'));
                $tarif->setNatureprix("prixttc");
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($tarif);
                $entityManager->flush();
            } */
            $this->addFlash('success', 'Article enregistré avec success!');
            return $this->redirectToRoute('article_index');
        }
    }

    /**
     * @Route("/{id}", name="article_show", methods={"GET"})
     */
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="article_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Article $article, FamilleRepository $familleRepository, TypesuiviRepository $typesuiviRepository): Response
    {
        $article->setReference($request->request->get('reference2'));
        $article->setFamilles($familleRepository->find($request->request->get('famille2')));
        $article->setTypesuivis($typesuiviRepository->find($request->request->get('suivi2')));
        $article->setDesignation($request->request->get('designation2'));
        $article->setPrix($request->request->get('prix2'));
        $article->setPrixventeht($request->request->get('prixventeht2'));
        $article->setPrixventettc($request->request->get('prixventettc2'));
        $article->setIsStocknegatif($request->request->get('isStocknegatif2'));

        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success', 'Article modifié avec success!');
        return $this->redirectToRoute('article_index');
    }


    /**
     * @Route("statut/{id}", name="article_statut")
     */
    public function statut(Article $article)
    {
        $article->setStatut(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

        return $this->redirectToRoute('article_index');
    }
}
