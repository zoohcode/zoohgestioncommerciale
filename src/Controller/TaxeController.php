<?php

namespace App\Controller;

use App\Entity\Taxe;
use App\Form\TaxeType;
use App\Repository\TaxeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/taxe")
 */
class TaxeController extends AbstractController
{
    /**
     * @Route("/", name="taxe_index", methods={"GET"})
     */
    public function index(TaxeRepository $taxeRepository): Response
    {
        return $this->render('taxe/index.html.twig', [
            'taxes' => $taxeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="taxe_new", methods={"GET","POST"})
     */
    public function new(Request $request,TaxeRepository $taxeRepository): Response
    {
        $taxe = new Taxe();
        $familleold = $taxeRepository->findBy([
            'intitule'=> $request->request->get('intitule1'),
            'taux'=> $request->request->get('taux1')
            ]);
        
        if ($familleold) {
            $this->addFlash('error', 'Cette taxe existe déjà');
            return $this->redirectToRoute('taxe_index');
        }else {
           
            $taxe->setIntitule( $request->request->get('intitule1'));
            $taxe->setTaux( $request->request->get('taux1'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($taxe);
            $entityManager->flush();
            $this->addFlash('success', 'Taxe créee avec success!');
            return $this->redirectToRoute('taxe_index');
        }
    }

    /**
     * @Route("/{id}", name="taxe_show", methods={"GET"})
     */
    public function show(Taxe $taxe): Response
    {
        return $this->render('taxe/show.html.twig', [
            'taxe' => $taxe,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="taxe_edit", methods={"GET","POST"})
     */
    public function edittaxe(Request $request, Taxe $taxe): Response
    {
        $taxe->setIntitule($request->request->get('intitule2'));
        $taxe->setTaux($request->request->get('taux2'));
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Taxe modifiée avec success!');
        return $this->redirectToRoute('taxe_index');
    }


    /**
     * @Route("/{id}", name="taxe_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Taxe $taxe): Response
    {
        
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($taxe);
        $entityManager->flush();
        $this->addFlash('success', 'suppression éffectuée avec success!');

        return $this->redirectToRoute('taxe_index');
    }
}
