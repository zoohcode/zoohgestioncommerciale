<?php

namespace App\Repository;

use App\Entity\Typefacture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Typefacture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Typefacture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Typefacture[]    findAll()
 * @method Typefacture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypefactureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Typefacture::class);
    }

    // /**
    //  * @return Typefacture[] Returns an array of Typefacture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Typefacture
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
