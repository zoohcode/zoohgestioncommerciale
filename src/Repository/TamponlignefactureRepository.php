<?php

namespace App\Repository;

use App\Entity\Tamponlignefacture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tamponlignefacture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tamponlignefacture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tamponlignefacture[]    findAll()
 * @method Tamponlignefacture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TamponlignefactureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tamponlignefacture::class);
    }

    // /**
    //  * @return Tamponlignefacture[] Returns an array of Tamponlignefacture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tamponlignefacture
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function delTampLigne()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "DELETE from tamponlignefacture";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return null;
    }
}
