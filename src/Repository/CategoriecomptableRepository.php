<?php

namespace App\Repository;

use App\Entity\Categoriecomptable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Categoriecomptable|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categoriecomptable|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categoriecomptable[]    findAll()
 * @method Categoriecomptable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriecomptableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categoriecomptable::class);
    }

    // /**
    //  * @return Categoriecomptable[] Returns an array of Categoriecomptable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Categoriecomptable
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
