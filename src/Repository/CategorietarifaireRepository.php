<?php

namespace App\Repository;

use App\Entity\Categorietarifaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Categorietarifaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categorietarifaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categorietarifaire[]    findAll()
 * @method Categorietarifaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorietarifaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categorietarifaire::class);
    }

    // /**
    //  * @return Categorietarifaire[] Returns an array of Categorietarifaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Categorietarifaire
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
