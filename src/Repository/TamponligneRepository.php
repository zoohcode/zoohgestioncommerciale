<?php

namespace App\Repository;

use App\Entity\Tamponligne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tamponligne|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tamponligne|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tamponligne[]    findAll()
 * @method Tamponligne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TamponligneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tamponligne::class);
    }

    // /**
    //  * @return Tamponligne[] Returns an array of Tamponligne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tamponligne
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
