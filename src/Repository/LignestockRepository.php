<?php

namespace App\Repository;

use App\Entity\Lignestock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lignestock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lignestock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lignestock[]    findAll()
 * @method Lignestock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LignestockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lignestock::class);
    }

    // /**
    //  * @return Lignestock[] Returns an array of Lignestock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lignestock
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function infoLot($article, $depot)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = " SELECT * 
        FROM lignestock 
        WHERE CONCAT(nlot,id) in (
        SELECT CONCAT(nlot,MAX(id))
        FROM `lignestock`
        WHERE `article` = '$article'
        AND `depot` = $depot
        GROUP by nlot
        )
        AND qtedispo <> 0
     
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function infotransfert($depot)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = " SELECT * 
        FROM lignestock 
        WHERE CONCAT(nlot, id) in (
        SELECT CONCAT(nlot, MAX(id))
        FROM `lignestock`
        WHERE  `depot` = $depot
        GROUP by nlot
        )
        AND 
        AND qtedispo <> 0
     
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function infoSerie($article, $depot)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = " SELECT * 
        FROM lignestock 
        WHERE CONCAT(nserie,id) in (
        SELECT CONCAT(nserie,MAX(id))
        FROM `lignestock`
        WHERE `article` = '$article'
        AND `depot` = $depot
        GROUP by nserie
        )
        AND qtedispo <> 0
     
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function sumQteSortie($id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT SUM(quantite) AS quantite FROM lignestock WHERE stocks_id = $id ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function listarticle()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT DISTINCT article
                FROM `lignestock` 
                ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function articlecmup($article,$depot)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = " SELECT * 
                FROM lignestock 
                WHERE id in (
                SELECT MAX(id)
                FROM `lignestock`
                WHERE `article` = '$article'
                AND `depot` = '$depot'
                ) 
                ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function articleserie($article,$depot)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = " SELECT COUNT(article) AS article
                FROM `lignestock` 
                WHERE  article = '$article'
                AND depot = '$depot'
                AND qtedispo <> 0
                                ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function articlelot($article,$depot)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT IFNULL(SUM(qtedispo),0) AS qte
        FROM lignestock 
        WHERE CONCAT(nlot,id) in (
        SELECT CONCAT(nlot,MAX(id))
        FROM `lignestock`
        WHERE  article = '$article'
        AND depot = '$depot'
        GROUP by nlot
        ) 
        AND qtedispo <> 0
                                ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function listlot($article, $depot)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(" SELECT li 
                    FROM App\Entity\Lignestock li 
                    WHERE CONCAT(li.nlot,li.id) in (
                                SELECT CONCAT(l.nlot,MAX(l.id))
                                FROM App\Entity\Lignestock l
                                WHERE l.article = :article
                                AND l.depot = :depot
                                GROUP BY l.nlot
                                )
                    AND li.qtedispo <> 0 
            
                ");
          $query->setParameter('article',$article);
          $query->setParameter('depot', $depot);

        $products = $query->getResult();
        return $products;

    }
    public function listOfArticle()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT DISTINCT article
        FROM lignestock ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();

    }


}
